﻿using AcroMobileApp.Droid.Crypto.Implementations;
using AcroMobileApp.Droid.Crypto.Interfaces;

namespace AcroMobileApp.Droid.Crypto
{
    public static class CryptoManager
    {
        private static ICryptoImplementation cryptoImplementation;

        private static readonly object CryptoLock = new object();

        static CryptoManager()
        {
            if ((int)Android.OS.Build.VERSION.SdkInt >= 23)
            {
                cryptoImplementation = new AESCrypto();
            }
            else
            {
                cryptoImplementation = new AESCryptoPre23();
            }
        }

        public static string Encrypt(string cipherText)
        {
            lock (CryptoLock)
            {
                return cryptoImplementation.Encrypt(cipherText);
            }
        }

        public static string Decrypt(string encryptedBytes)
        {
            lock (CryptoLock)
            {
                return cryptoImplementation.Decrypt(encryptedBytes);
            }
        }
    }
}