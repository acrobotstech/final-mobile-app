﻿using Javax.Crypto;
using Javax.Crypto.Spec;
using Android.Security.Keystore;
using Java.Security;
using Android.Security;
using Javax.Security.Auth.X500;
using Java.Math;
using Java.Util;
using System.IO;
using System.Collections.Generic;
using Android.Content;
using System.Text;
using AcroMobileApp.Droid.Crypto.Interfaces;

namespace AcroMobileApp.Droid.Crypto.Implementations
{
    //This is our AES implementation targetting API levels below 23, 
    //Below 23 the keystore actually contains an RSA implementation, which is used to encrypt an AES key which is actually stored in SharedPreferences
    //Once this key is decrypted it is then used to decrypt the DB string.
    public class AESCryptoPre23 : ICryptoImplementation
    {
        private static readonly string ANDROID_KEYSTORE_NAME = "AndroidKeyStore";

        private static readonly string SHARED_PREFERENCES_NAME = "SharedPreferences";

        private static readonly string RSA_KEY_NAME = "RSAKey";

        private static readonly string AES_KEY_NAME = "AESKeyForAPI18";

        private static readonly string ANDROID_CIPHER_PROVIDER = "AndroidOpenSSL";

        private static readonly string BC_CIPHER_PROVIDER = "BC";

        private static readonly int RSA_KEY_EXPIRY_YEARS = 30;

        private static readonly string RSA_MODE = KeyProperties.KeyAlgorithmRsa + "/" + KeyProperties.BlockModeEcb + "/" + KeyProperties.EncryptionPaddingRsaPkcs1;

        private static readonly string AES_MODE = KeyProperties.KeyAlgorithmAes + "/" + KeyProperties.BlockModeEcb + "/" + KeyProperties.EncryptionPaddingPkcs7;

        private static KeyStore KeyStore;

        public AESCryptoPre23()
        {
            KeyStore = KeyStore.GetInstance(ANDROID_KEYSTORE_NAME);
            KeyStore.Load(null);

            GenerateRSAKey();

            GenerateAESKey();
        }

        private void GenerateRSAKey()
        {
            // Generate the RSA key pairs
            if (!KeyStore.ContainsAlias(RSA_KEY_NAME))
            {
                // Generate a key pair for encryption
                Calendar start = Calendar.Instance;
                Calendar end = Calendar.Instance;
                end.Add(CalendarField.Year, RSA_KEY_EXPIRY_YEARS);

                //Translated from https://medium.com/@ericfu/securely-storing-secrets-in-an-android-application-501f030ae5a3
#pragma warning disable CS0618 // Type or member is obsolete - used deliberately for backward compatibility
                KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(Android.App.Application.Context)
#pragma warning restore CS0618 // Type or member is obsolete
                        .SetAlias(RSA_KEY_NAME)
                        .SetSubject(new X500Principal("CN=" + RSA_KEY_NAME))
                        .SetSerialNumber(BigInteger.Ten)
                        .SetStartDate(start.Time)
                        .SetEndDate(end.Time)
                        .Build();
                KeyPairGenerator kpg = KeyPairGenerator.GetInstance(KeyProperties.KeyAlgorithmRsa, ANDROID_KEYSTORE_NAME);
                kpg.Initialize(spec);
                kpg.GenerateKeyPair();
            }
        }

        private byte[] RsaEncrypt(byte[] secret)
        {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)KeyStore.GetEntry(RSA_KEY_NAME, null);
            // Encrypt the text
            Cipher inputCipher = Cipher.GetInstance(RSA_MODE, ANDROID_CIPHER_PROVIDER);
            inputCipher.Init(CipherMode.EncryptMode, privateKeyEntry.Certificate.PublicKey);

            MemoryStream stream = new MemoryStream();

            CipherOutputStream cipherOutputStream = new CipherOutputStream(stream, inputCipher);
            cipherOutputStream.Write(secret);
            cipherOutputStream.Close();

            byte[] vals = stream.ToArray();
            return vals;
        }

        private byte[] RsaDecrypt(byte[] encrypted)
        {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)KeyStore.GetEntry(RSA_KEY_NAME, null);
            Cipher output = Cipher.GetInstance(RSA_MODE, ANDROID_CIPHER_PROVIDER);
            output.Init(CipherMode.DecryptMode, privateKeyEntry.PrivateKey);
            CipherInputStream cipherInputStream = new CipherInputStream(new MemoryStream(encrypted), output);
            List<byte> values = new List<byte>();
            int nextByte;
            while ((nextByte = cipherInputStream.Read()) != -1)
            {
                values.Add((byte)nextByte);
            }

            byte[] bytes = new byte[values.Count];
            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = values[i];
            }
            return bytes;
        }

        private void GenerateAESKey()
        {
            ISharedPreferences pref = Android.App.Application.Context.GetSharedPreferences(SHARED_PREFERENCES_NAME, FileCreationMode.Private);
            string enryptedKeyB64 = pref.GetString(AES_KEY_NAME, null);
            if (enryptedKeyB64 == null)
            {
                byte[] key = new byte[16];
                SecureRandom secureRandom = new SecureRandom();
                secureRandom.NextBytes(key);
                byte[] encryptedKey = RsaEncrypt(key);
                enryptedKeyB64 = System.Convert.ToBase64String(encryptedKey);
                ISharedPreferencesEditor edit = pref.Edit();
                edit.PutString(AES_KEY_NAME, enryptedKeyB64);
                edit.Commit();
            }
        }

        private SecretKeySpec GetSecretKey()
        {
            ISharedPreferences pref = Android.App.Application.Context.GetSharedPreferences(SHARED_PREFERENCES_NAME, FileCreationMode.Private);
            string enryptedKeyB64 = pref.GetString(AES_KEY_NAME, null);
            // need to check null, omitted here
            byte[] encryptedKey = System.Convert.FromBase64String(enryptedKeyB64);
            byte[] key = RsaDecrypt(encryptedKey);
            return new SecretKeySpec(key, "AES");
        }

        public string Encrypt(string input)
        {
            Cipher c = Cipher.GetInstance(AES_MODE, BC_CIPHER_PROVIDER);
            c.Init(CipherMode.EncryptMode, GetSecretKey());
            byte[] encodedBytes = c.DoFinal(Encoding.ASCII.GetBytes(input));
            string encryptedBase64Encoded = System.Convert.ToBase64String(encodedBytes);
            return encryptedBase64Encoded;
        }

        public string Decrypt(string encrypted)
        {
            Cipher c = Cipher.GetInstance(AES_MODE, BC_CIPHER_PROVIDER);
            c.Init(CipherMode.DecryptMode, GetSecretKey());
            byte[] decodedBytes = c.DoFinal(System.Convert.FromBase64String(encrypted));
            return Encoding.ASCII.GetString(decodedBytes);
        }
    }
}