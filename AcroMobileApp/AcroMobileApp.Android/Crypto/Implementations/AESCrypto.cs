﻿using System;
using System.Text;

using Javax.Crypto;
using Javax.Crypto.Spec;
using Android.Security.Keystore;
using Java.Security;
using AcroMobileApp.Droid.Crypto.Interfaces;

namespace AcroMobileApp.Droid.Crypto.Implementations
{
    // An AES implementation targetting API level 23 and above
    public class AESCrypto : ICryptoImplementation
    {
        private static KeyStore KeyStore;

        private static readonly string KEYSTORE_NAME = "AndroidKeyStore";

        private static readonly string KEY_NAME = "DBKey";

        //An IV is an initialization vector (https://en.wikipedia.org/wiki/Initialization_vector)
        //We are using a fixed IV to reduce the complexity of our AES implementation as we don't need the properties given by a dynamic IV
        private static readonly string FIXED_IV = @"飗ᅄﱥ뀀";

        private static readonly string AES_MODE = KeyProperties.KeyAlgorithmAes + "/" + KeyProperties.BlockModeGcm + "/" + KeyProperties.EncryptionPaddingNone;

        public AESCrypto()
        {
            KeyStore = KeyStore.GetInstance(KEYSTORE_NAME);
            KeyStore.Load(null);

            GenerateAESPrivateKey();
        }

        private void GenerateAESPrivateKey()
        {
            if (!KeyStore.ContainsAlias(KEY_NAME))
            {
                KeyGenerator keyGenerator = KeyGenerator.GetInstance(KeyProperties.KeyAlgorithmAes, KEYSTORE_NAME);

                KeyGenParameterSpec keyGenParameterSpec = new KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyStorePurpose.Encrypt | KeyStorePurpose.Decrypt)
                        .SetBlockModes(KeyProperties.BlockModeGcm)
                        .SetEncryptionPaddings(KeyProperties.EncryptionPaddingNone)
                        //Bare this in mind if you plan on sending data over a network, refer to https://en.wikipedia.org/wiki/Ciphertext_indistinguishability 
                        //Used here to reduce the complexity of our implementation as we don't need ciphertext indistinguishability 
                        .SetRandomizedEncryptionRequired(false)
                        .Build();

                keyGenerator.Init(keyGenParameterSpec);
                keyGenerator.GenerateKey();
            }
        }

        public string Encrypt(string cipherText)
        {
            KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry)KeyStore.GetEntry(KEY_NAME, null);

            Cipher cipher = Cipher.GetInstance(AES_MODE);
            cipher.Init(CipherMode.EncryptMode, secretKeyEntry.SecretKey, new GCMParameterSpec(128, Encoding.UTF8.GetBytes(FIXED_IV)));

            var encryptedBytes = cipher.DoFinal(Encoding.ASCII.GetBytes(cipherText));

            return System.Convert.ToBase64String(encryptedBytes);
        }

        public string Decrypt(string encryptedString)
        {
            KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry)KeyStore.GetEntry(KEY_NAME, null);

            Cipher cipher = Cipher.GetInstance(AES_MODE);
            cipher.Init(CipherMode.DecryptMode, secretKeyEntry.SecretKey, new GCMParameterSpec(128, Encoding.UTF8.GetBytes(FIXED_IV)));

            byte[] decodedData = cipher.DoFinal(System.Convert.FromBase64String(encryptedString));

            return Encoding.ASCII.GetString(decodedData);
        }
    }
}