﻿namespace AcroMobileApp.Droid.Crypto.Interfaces
{
    //This interface is used to decouple our cryto implementation from the calling code
    //Ultimatly we don't mind what is happening under the hood we just care it can encrypt and decrypt
    public interface ICryptoImplementation
    {
        string Encrypt(string input);

        string Decrypt(string encrypted);
    }
}