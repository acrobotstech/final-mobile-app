﻿using System;
using System.IO;
using SQLiteAsync;
using SqliteAsync.Droid;
using Android.Content;
using Xamarin.Forms;
using AcroMobileApp.Droid.Crypto;

[assembly: Dependency(typeof(SQLite_Android))]
namespace SqliteAsync.Droid
{
    /// <summary>
    /// Android-specific wrapper for SqLite
    /// </summary>
    public class SQLite_Android : SQLiteBase
    {
        private static readonly string SHARED_PREFERENCES_NAME = "SharedPreferences";

        protected override string getPragmaKey()
        {
            ISharedPreferences pref = Android.App.Application.Context.GetSharedPreferences(SHARED_PREFERENCES_NAME, FileCreationMode.Private);

            string pragmaKey = pref.GetString(PRAGMA_KEY_NAME, null);

            if (pragmaKey == null)
            {
                var key = generatePragmaKey();

                ISharedPreferencesEditor edit = pref.Edit();
                edit.PutString(PRAGMA_KEY_NAME, CryptoManager.Encrypt(key));
                edit.Commit();

                return key;
            }

            return CryptoManager.Decrypt(pragmaKey);
        }

        public override void DeleteDatabase()
        {
            // Delete the DB file
            var path = GetDatabasePath();
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            // Delete encrypted pragma key
            ISharedPreferences pref = Android.App.Application.Context.GetSharedPreferences(SHARED_PREFERENCES_NAME, FileCreationMode.Private);
            ISharedPreferencesEditor edit = pref.Edit();
            edit.Remove(PRAGMA_KEY_NAME);
            edit.Commit();
        }


        protected override string GetDatabasePath()
        {
            const string sqliteFilename = "SelfServiceDb.db3";

            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
            var path = Path.Combine(documentsPath, sqliteFilename);

            return path;
        }
    }
}