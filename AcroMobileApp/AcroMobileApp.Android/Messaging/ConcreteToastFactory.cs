﻿using AcroMobileApp.Droid.Messaging;
using Android.Widget;
using Xamarin.Forms;

[assembly: Dependency(typeof(ConcreteToastFactory))]
namespace AcroMobileApp.Droid.Messaging
{
    public class ConcreteToastFactory : IDisplayMessage
    {
        public void DisplayMessage(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Long).Show();
        }
    }
}
