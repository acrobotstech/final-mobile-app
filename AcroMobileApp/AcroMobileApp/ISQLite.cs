﻿using System;
using SQLite;
using Xamarin.Forms.Internals;

namespace SQLiteAsync
{
    [Xamarin.Forms.Internals.Preserve(AllMembers = true)]
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
        void DeleteDatabase();
    }
}
