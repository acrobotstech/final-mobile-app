﻿using System;
using SQLite;

namespace AcroMobileApp.BusinessLayer.Contracts
{
    [Xamarin.Forms.Internals.Preserve(AllMembers = true)]
    public abstract class BusinessEntityBase
    {
        [PrimaryKey]
        public int Id { get; set; }
    }
}
