﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace acrobots.mobile.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class PostPayment
    {
        public decimal transAmount { get; set; }

        public PostPayment(decimal transAmount)
        {
            this.transAmount = transAmount;
        }
    }
}
