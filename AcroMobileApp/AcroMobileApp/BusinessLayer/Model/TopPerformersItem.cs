﻿using System;
namespace AcroMobileApp.BusinessLayer.Model
{
    public class TopPerformersItem
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string TotalDisbursals { get; set; }
        public string PhotoPath { get; set; }
    }
}
