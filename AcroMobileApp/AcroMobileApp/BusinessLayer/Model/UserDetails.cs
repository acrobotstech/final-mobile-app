﻿using System;
using Xamarin.Forms.Internals;

namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers=true)]
    public class UserDetails
    {
        public string RegisterAs { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public int BranchId { get; set; }

        public UserDetails(string resgisterAs, string name, string email, string mobileNumber, int branchId)
        {
            RegisterAs = resgisterAs;
            Name = name;
            Email = email;
            MobileNumber = mobileNumber;
            BranchId = branchId;
        }
    }
}
