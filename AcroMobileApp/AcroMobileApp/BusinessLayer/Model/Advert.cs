﻿using Xamarin.Forms.Internals;

namespace acrobots.mobile.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class Advert
    {
        public string Url { get; set; }
        public string Image { get; set; }
    }
}