﻿using System;
using Xamarin.Forms.Internals;

namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class BranchDetailItem
    {
        public int BranchId { get; set; }
        public string Name { get; set; }
    }
}
