﻿using System;
using Xamarin.Forms.Internals;
namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class SaveNoteRequest
    {

        public string Notes { get; set; }
        public string LeadId { get; set; }

        public SaveNoteRequest(string notes, string leadId)
        {
            this.Notes = notes;
            this.LeadId = leadId;
        }
    }
}
