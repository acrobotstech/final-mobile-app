﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.Internals;

namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class LeadInfoItem
    {
        public List<LeadContactInfo> ContactInfo { get; set; }
        public List<LeadPersonalInfo> PersonalInfo { get; set; }
        public List<LeadLoginInfo> LoginInfo { get; set; }
    }
}
