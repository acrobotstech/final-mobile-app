﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AcroMobileApp.BusinessLayer.Model
{
   
  
    public class ArrayOfNote
    {
        public int LeadNotesId { get; set; }
        public int LeadId { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }

    public class LeadDetailsResponse : ApiResponse
    {
    
        public int LeadId { get; set; }
        public string Name { get; set; }
        public Double LoanAmount { get; set; }
        public string BankRMName { get; set; }        
        public string MobileNo { get; set; }
        public string BankRMMobile { get; set; }
        public string ProductName { get; set; }
        public string City { get; set; }
        public Double CommissionAmount { get; set; }
        public int daysCnt { get; set; }
        public List<ArrayOfNote> ArrayOfNotes { get; set; }
    }
}
