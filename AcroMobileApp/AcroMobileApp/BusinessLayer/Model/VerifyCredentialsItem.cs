﻿using System;
namespace AcroMobileApp.BusinessLayer.Model
{
    public class VerifyCredentialsItem
    {
        public string MobileNumber { get; set; }
        public string OTP { get; set; }

        public VerifyCredentialsItem(string mobileNumber, string otp)
        {
            this.MobileNumber = mobileNumber;
            this.OTP = otp;
        }
    }
}
