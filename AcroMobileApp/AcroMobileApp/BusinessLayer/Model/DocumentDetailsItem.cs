﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AcroMobileApp.BusinessLayer.Model
{
    public class DocumentDetailsItem
    {
        public String DocCategoryCode { get; set; }
        public List<DocumentDetails> DocumentDetails { get; set; } = new List<DocumentDetails>();

        public DocumentDetailsItem(String docCategoryCode)
        {

            DocCategoryCode = docCategoryCode;
        }


        public void addDocument(DocumentDetails details)
        {
            DocumentDetails.Add(details);
        }
    }
}
