﻿namespace acrobots.BusinessLayer.Model
{
    public class PaymentCode
    {
        public string Code { get; set; }
        public string ImageName { get; set; }
    }
}