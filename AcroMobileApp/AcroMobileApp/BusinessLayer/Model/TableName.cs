﻿using Xamarin.Forms.Internals;

namespace acrobots.mobile.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class TableName
    {
        public TableName() { }
        public string Name { get; set; }
    }
}
