﻿using Xamarin.Forms.Internals;
namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class ProductDetailItem
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}