﻿using System;
namespace AcroMobileApp.BusinessLayer.Model
{
    public class CreateLeadDetailItem
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string BranchId { get; set; }
        public string LoanAmount { get; set; }

        public CreateLeadDetailItem(string productId, string name, string emailId, string mobileNo, string branchId, string loanAmount)
        {
            this.ProductId = productId;
            this.Name = name;
            this.EmailId = emailId;
            this.MobileNo = mobileNo;
            this.BranchId = branchId;
            this.LoanAmount = loanAmount;
        }
    }
}
