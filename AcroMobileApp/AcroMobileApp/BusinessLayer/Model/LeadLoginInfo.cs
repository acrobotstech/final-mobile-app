﻿using System;
namespace AcroMobileApp.BusinessLayer.Model
{
    public class LeadLoginInfo
    {
        public int LeadId { get; set; }
        public string RequestType { get; set; }
        public string BankRMName { get; set; }
        public string FileLoginDate { get; set; }
        public string BankApplicationId { get; set; }
        public long BankRMMobile { get; set; }
        public string BankRMEmail { get; set; }
        public string Photo { get; set; }
        public string BankId { get; set; }
        public string BankBranch { get; set; }
        public string DocPickupStatus { get; set; }

        public LeadLoginInfo(int leadId, string requestType, string bankRMName, string fileLoginDate, string bankApplicationId, long bankRMMobile, string bankRMEmail, string photo, string bankId, string bankBranch, string docPickupStatus)
        {
            this.LeadId = leadId;
            this.RequestType = requestType;
            this.BankRMName = bankRMName;
            this.FileLoginDate = fileLoginDate;
            this.BankApplicationId = bankApplicationId;
            this.BankRMMobile = bankRMMobile;
            this.BankRMEmail = bankRMEmail;
            this.Photo = photo;
            this.BankId = bankId;
            this.BankBranch = bankBranch;
            this.DocPickupStatus = docPickupStatus;
        }
    }
}
