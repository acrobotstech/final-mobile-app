﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace acrobots.BusinessLayer.Model
{
    public class PaymentCodeConfig
    {
        [XmlElement("PaymentCode")]
        public List<PaymentCode> paymentCodeList = new List<PaymentCode>();
    }
}