﻿using System;
using Xamarin.Forms.Internals;

namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers=true)]
    public class UpdateLeadStatus
    {
        public string LeadId { get; set; }
        public string LeadReason { get; set; }
        public string LeadComments { get; set; }
        public string LeadStatus { get; set; }
        

        public UpdateLeadStatus(string leadId, string leadReason, string leadComments, string leadStatus)
        {
            LeadId = leadId;
            LeadReason = leadReason;
            LeadComments = leadComments;
            LeadStatus = leadStatus;            
        }
    }
}
