﻿using System;
using Xamarin.Forms.Internals;

namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class CityDetailItem
    {
        public int CityId { get; set; }
        public string Name { get; set; }
    }
}
