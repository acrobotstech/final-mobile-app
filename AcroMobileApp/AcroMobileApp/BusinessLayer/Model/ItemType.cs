﻿using Xamarin.Forms.Internals;

namespace acrobots.mobile.BusinessLayer
{
    [Preserve(AllMembers = true)]
    public enum ItemType
    {
        BillItem,
        PaymentItem,
        UsageItem,
        UserItem
    }
}