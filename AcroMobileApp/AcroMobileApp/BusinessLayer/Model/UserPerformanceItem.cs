﻿using System;
namespace AcroMobileApp.BusinessLayer.Model
{
    public class UserPerformanceItem
    {
        public int InProgress { get; set; }
        public int Aprroved { get; set; }
        public int Disbursed { get; set; }
        public int Rejected { get; set; }
        public int TotalEarning { get; set; }
    }
}
