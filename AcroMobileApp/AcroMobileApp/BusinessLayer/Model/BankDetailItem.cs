﻿using System;
namespace AcroMobileApp.BusinessLayer.Model
{
    public class BankDetailItem
    {
        public int BankId { get; set; }
        public string Name { get; set; }
    }
}
