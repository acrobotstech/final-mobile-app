﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AcroMobileApp.BusinessLayer.Model
{

    public class DocCategory
    {
        public int DocCategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class DocumentDetails
    {
        public int LeadDocId { get; set; }
        public int DocumentId { get; set; }
        public string CategoryName { get; set; }
        public string DocumentName { get; set; }
        public string FileName { get; set; }
        public string FileFullUrl { get; set; }
    }

    public class DocumentDetailsResponse : ApiResponse
    {      
        public int LeadId { get; set; }
        public string Name { get; set; }
        public List<DocCategory> DocCategories { get; set; }
        public List<DocumentDetails> DocumentDetails { get; set; }
    }
}
