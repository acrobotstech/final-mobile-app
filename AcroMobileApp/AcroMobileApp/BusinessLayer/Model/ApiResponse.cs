﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class ApiResponse 
    {
        public Messages messages { get; set; }
        public int responseCode { get; set; } = 1; //To prevent returning 0 for success by default
        public int Id { get; set; } = -1;
    }

}
