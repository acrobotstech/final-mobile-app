﻿using System;
using Xamarin.Forms.Internals;

namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class VerifyCredentialsResponse
    {
        public AcroMobileApp.BusinessLayer.Model.Messages messages { get; set; }
        public int responseCode { get; set; } = 1; //To prevent returning 0 for success by default
        public string accessToken { get; set; }
        public string token_type { get; set; }
        public long expires_in { get; set; }
    }
}
