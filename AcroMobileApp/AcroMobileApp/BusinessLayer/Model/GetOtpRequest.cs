﻿using System;
using Xamarin.Forms.Internals;
namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class GetOtpRequest
    {
        public string MobileNumber { get; set; }

        public GetOtpRequest(string mobileNumber)
        {
            this.MobileNumber = mobileNumber;
        }
    }
}
