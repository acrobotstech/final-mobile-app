﻿using System;
using AcroMobileApp.BusinessLayer.Contracts;
using Xamarin.Forms.Internals;

namespace AcroMobileApp.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class Auth : BusinessEntityBase
    {
        public string accessToken { get; set; }
        public DateTime expiresAt { get; set; }
        public string tokenType { get; set; }
    }
}
