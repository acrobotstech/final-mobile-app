﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AcroMobileApp.BusinessLayer.Model
{
    public class MeetingDetails
    {
        
        public DateTime Date { get; set; }
        public TimeSpan TimeSlot { get; set; }
        public string Address { get; set; }
        public int LeadId { get; set; }

        public MeetingDetails(int leadId, DateTime date, TimeSpan timeSlot, string address)
        {
         
            LeadId = leadId;
            Date = date;
            TimeSlot = timeSlot;
            Address = address;
        }
    }
}
