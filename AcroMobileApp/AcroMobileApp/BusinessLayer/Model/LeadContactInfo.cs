﻿using System;
namespace AcroMobileApp.BusinessLayer.Model
{
    public class LeadContactInfo
    {
        public int LeadId { get; set; }
        public string RequestType { get; set; }
        public string FullName { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public string City { get; set; }
        public double LoanAmount { get; set; }
        public int TenureInMonths { get; set; }

        public LeadContactInfo(int leadId, string requestType, string fullName, string mobileNo, string emailId, string city, double loanAmount, int tenureInMonths)
        {
            this.LeadId = leadId;
            this.RequestType = requestType;
            this.FullName = fullName;
            this.MobileNo = mobileNo;
            this.EmailId = emailId;
            this.City = city;
            this.LoanAmount = loanAmount;
            this.TenureInMonths = tenureInMonths;
        }
    }
}
