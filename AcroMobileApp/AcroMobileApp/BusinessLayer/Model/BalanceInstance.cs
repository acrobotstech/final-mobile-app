﻿using System;
using acrobots.ResX;
using Xamarin.Forms.Internals;

namespace acrobots.mobile.BusinessLayer
{
    [Preserve(AllMembers = true)]
    public class BalanceInstance
    {
        public long FromDt { get; set; }
        public long FromDtUtcOffset { get; set; }
        public long ToDt { get; set; }
        public long ToDtUtcOffset { get; set; }
        public decimal InitialValue { get; set; }
        public decimal Value { get; set; }
        public bool ExpireFlag { get; set; }
        public string BalGroupCd { get; set; }



        public string GetExpiryText()
        {
            if (ToDt == 0)
            {
                return AppResources.NoExpiry;
            }

            TimeSpan time = TimeSpan.FromMilliseconds(ToDt);
            DateTime expiryDateTime = new DateTime(1970, 1, 1) + time;
            string dtmString = expiryDateTime.Date.ToString().Split(' ')[0];

            if (ExpireFlag)
            {
                return AppResources.ExpiresOn + " " + dtmString;
            }

            if (BalGroupCd.Equals("ALLOW"))
            {
                return AppResources.RenewsOn + " " + dtmString;
            }

            return AppResources.RecursOn + " " + dtmString;
        }
    }
}