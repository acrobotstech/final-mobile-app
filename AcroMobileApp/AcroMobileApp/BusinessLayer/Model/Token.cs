﻿using Xamarin.Forms.Internals;

namespace acrobots.mobile.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class Token
    {
        public string token { get; set; }
        
        // Flag to indicate service level access only
        public string serviceOnlyFlag { get; set; }

        public bool ServiceAccessOnly
        {
            get
            {
                return serviceOnlyFlag == "1";
            }
        }

        public string GetAuthString()
        {
            return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(token + ":"));
        }
    }
}