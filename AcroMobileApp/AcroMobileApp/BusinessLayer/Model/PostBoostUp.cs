﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace acrobots.mobile.BusinessLayer.Model
{
    [Preserve(AllMembers = true)]
    public class PostBoostUp
    {
        public string tariffCode { get; set; }
        public string tariffSuffix { get; set; }
        public int tariffVersion { get; set; }
        public string accessNo { get; set; }

        public PostBoostUp(string tariffCode, string tariffSuffix, int tariffVersion, string accessNo)
        {
            this.tariffCode = tariffCode;
            this.tariffSuffix = tariffSuffix;
            this.tariffVersion = tariffVersion;
            this.accessNo = accessNo;
        }
      
    }
}
