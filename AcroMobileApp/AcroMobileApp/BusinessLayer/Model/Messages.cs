﻿using System;

namespace AcroMobileApp.BusinessLayer.Model
{
    public class Messages
    {
		public string messageType { get; set; }
        public string messageDescription { get; set; }
	}
}
