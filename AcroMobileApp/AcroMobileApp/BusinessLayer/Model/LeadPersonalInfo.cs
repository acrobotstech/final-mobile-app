﻿using System;
namespace AcroMobileApp.BusinessLayer.Model
{
    public class LeadPersonalInfo
    {
        public int LeadId { get; set; }
        public string RequestType { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string PancardNo { get; set; }
        public string AadharNo { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }

        public LeadPersonalInfo(int leadId, string requestType, DateTime dob, string gender, string panCardNo, string aadharNo, string address, string pinCode)
        {
            this.LeadId = leadId;
            this.RequestType = requestType;
            this.DOB = dob;
            this.Gender = gender;
            this.PancardNo = panCardNo;
            this.AadharNo = aadharNo;
            this.Address = address;
            this.PinCode = pinCode;
        }
    }
}
