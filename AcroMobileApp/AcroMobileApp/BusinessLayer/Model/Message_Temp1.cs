﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace acrobots.mobile.BusinessLayer.Model
{
    public class Message
    {
        private String messageResourceId;

        public String getMessageResourceId() { return this.messageResourceId; }

        public void setMessageResourceId(String messageResourceId) { this.messageResourceId = messageResourceId; }

        private List<String> messageSubstitutions;

        public List<String> getMessageSubstitutions() { return this.messageSubstitutions; }

        public void setMessageSubstitutions(List<String> messageSubstitutions) { this.messageSubstitutions = messageSubstitutions; }

        private int type;

        public int getType() { return this.type; }

        public void setType(int type) { this.type = type; }
    }
}
