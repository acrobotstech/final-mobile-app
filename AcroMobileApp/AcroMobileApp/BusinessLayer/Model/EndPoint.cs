﻿using Xamarin.Forms.Internals;

namespace acrobots.mobile.BusinessLayer
{
    [Preserve(AllMembers = true)]
    public class EndPoint
    {
        public string Type { get; set; }
        public string Data { get; set; }
    }
}