﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using acrobots.BusinessLayer.Model;
using acrobots.mobile.Web;
using AcroMobileApp.BusinessLayer.Model;
using AcroMobileApp.Models;
using SQLiteAsync.Services;

namespace AcroMobileApp.BusinessLayer
{
    //Provides the UI access to the model
    public static class BusinessManager
    {
        private static readonly RestService Rest = new RestService();

        private static readonly DatabaseService DatabaseService = DatabaseService.Instance;
        private static ObservableCollection<TopPerformersItem> topPerfomersList = null;
        private static List<ProductTypeDetailItem> productTypes= null;
        private static List<CityDetailItem> cities = null;
        private static readonly object RegisterUserLock = new object();
        private static readonly object ChangeStatusLock = new object();
        private static readonly object CitiesLock = new object();
        private static readonly object BranchesLock = new object();
        private static readonly object GetOtpLock = new object();
        private static readonly object ResendOtpLock = new object();
        private static readonly object VerifyOtpLock = new object();
        private static readonly object ProductTypesLock = new object();
        private static readonly object ProductsLock = new object();
        private static readonly object CreateLeadLock = new object();
        private static readonly object UserPerformanceLock = new object();
        private static readonly object TopPerformersLock = new object();
        private static readonly object LeadInfoLock = new object();
        private static readonly object GetViewLeadsLock = new object();
        private static readonly object BanksLock = new object();

        public static bool ValidateToken()
        {
            var authList = DatabaseService.GetAllItems<Auth>();

            if (authList != null && authList.Count > 0 && !string.IsNullOrEmpty(authList[0].accessToken))
                return true;

            return false;
        }

        public static Auth GetToken()
        {
            return DatabaseService.GetAllItems<Auth>().Count > 0 ? DatabaseService.GetAllItems<Auth>()[0] : null;
        }

        public static List<CityDetailItem> GetCities()
        {
            lock (CitiesLock)
            {
                if(cities == null)
                cities = Rest.MakeGetRestCall<List<CityDetailItem>>("/user/GetAllCity");

                return cities;
            }
        }

        public static List<BranchDetailItem> GetBranches(int cityId)
        {
            lock (BranchesLock)
            {
                return Rest.MakeGetRestCall<List<BranchDetailItem>>("/user/GetBranchByCity?CityId=" + cityId);
            }
        }

        public static ApiResponse RegisterUser(string registerAs, string name, string email, string mobileNumber, int branchId)
        {
            lock (RegisterUserLock)
            {
                return Rest.MakePostRestCall<ApiResponse>("/user/PostRegisterUser", new UserDetails(registerAs, name, email, mobileNumber, branchId));
            }
        }

        public static ApiResponse ChangeLeadStatus(String leadId, String leadReason,  String leadComments, String leadStatus)
        {
            lock (ChangeStatusLock)
            {
                return Rest.MakePostRestCall<ApiResponse>("/Auth/UpdateLeadStatus", new UpdateLeadStatus(leadId, leadReason, leadComments, leadStatus));
            }
        }

        public static ApiResponse GetOtpForLogin(string mobileNumber)
        {
            lock (GetOtpLock)
            {
                return Rest.MakePostRestCall<ApiResponse>("/user/PostsendLoginSms", new GetOtpRequest(mobileNumber));
            }
        }

        public static ApiResponse ResendOtpForLogin(string mobileNumber)
        {
            lock (ResendOtpLock)
            {
                return Rest.MakePostRestCall<ApiResponse>("/user/PostsendLoginResendSms", new GetOtpRequest(mobileNumber));
            }
        }

        public static VerifyCredentialsResponse VerifyOtpForLogin(string mobileNumber, string otp)
        {
            lock (VerifyOtpLock)
            {
                return Rest.MakePostRestCall<VerifyCredentialsResponse>("/user/PostVerifyLogin", new VerifyCredentialsItem(mobileNumber, otp));
            }
        }

        public static List<ProductTypeDetailItem> GetProductTypes()
        {
            lock (ProductTypesLock)
            {
                if(productTypes == null)
                    productTypes = Rest.MakeGetRestCall<List<ProductTypeDetailItem>>("/user/GetAllProduct?RequestType=ProductType&Paramtype=");

                return productTypes;
            }
        }

        public static ObservableCollection<String> GetAllChangeStatusReasons()
        {
            lock (ProductTypesLock)
            {
                return Rest.MakeGetRestCall<ObservableCollection<String>>("/User/GetAllReasons/");
            }
        }

        public static DocumentDetailsResponse GetLeadDocumentDetails(int leadId)
        {
            lock (ProductTypesLock)
            {
                return Rest.MakeGetRestCall<DocumentDetailsResponse>("/Auth/GetLeadDocumentDetails/?LeadId=" + leadId);
            }
        }

        public static ObservableCollection<LeadDetailsItem> GetViewLeads(int pageNo, int pageSize, String leadStatus)
        {
            lock (GetViewLeadsLock)
            {
                String url = "/Auth/GetMyLeads?pageNumber=" + pageNo + "&pageSize=" + pageSize + "&LeadStatus=" + leadStatus;
                return Rest.MakeGetRestCall<ObservableCollection<LeadDetailsItem>>(url);
            }
        }

        public static LeadDetailsResponse GetCompleteLeadDetails(int leadId)
        {
            lock (GetViewLeadsLock)
            {
                String url = "/Auth/GetLeadDetails/?LeadId=" + leadId;
                return Rest.MakeGetRestCall<LeadDetailsResponse>(url);
            }
        }

        public static ApiResponse SaveNote(int leadId, String note)
        {
            
                lock (VerifyOtpLock)
                {
                    return Rest.MakePostRestCall<ApiResponse>("/Auth/AddLeadNote", new SaveNoteRequest(note, leadId.ToString()));
                }
            
        }


        public static List<ProductDetailItem> GetProducts(string productId)
        {
            lock (ProductsLock)
            {
                return Rest.MakeGetRestCall<List<ProductDetailItem>>("/user/GetAllProduct?RequestType=Product&Paramtype=" + productId);
            }
        }

        public static ApiResponse CreateLead(CreateLeadDetailItem item)
        {
            lock (CreateLeadLock)
            {
                return Rest.MakePostRestCall<ApiResponse>("/Auth/PostCreateLead", item);
            }
        }

        public static UserPerformanceItem GetUserPerformance()
        {
            lock (UserPerformanceLock)
            {
                return Rest.MakeGetRestCall<UserPerformanceItem>("/Auth/UserPerformanceOverview?RequestType=lead&Paramtype=count");
            }
        }

        public static ObservableCollection<TopPerformersItem> GetTopPerformers()
        {

            if(topPerfomersList != null)
            {
                return topPerfomersList;
            }
            lock (TopPerformersLock)
            {
                topPerfomersList = Rest.MakeGetRestCall<ObservableCollection<TopPerformersItem>>("/Auth/GetTopPerformers?RequestType=Performers&Paramtype=10");
                return topPerfomersList; 
            }
        }

        public static LeadInfoItem GetLeadInfo(int leadId)
        {
            lock (LeadInfoLock)
            {
                return Rest.MakeGetRestCall<LeadInfoItem>("/Auth/GetLeadAndApplicationDetails?LeadId=" + leadId);
            }
        }

        public static ApiResponse SaveLeadContactInfo(LeadContactInfo item)
        {
            lock (LeadInfoLock)
            {
                return Rest.MakePostRestCall<ApiResponse>("/Auth/PostLeadInfo", item);
            }
        }

        public static ApiResponse SaveLeadPersonalInfo(LeadPersonalInfo item)
        {
            lock (LeadInfoLock)
            {
                return Rest.MakePostRestCall<ApiResponse>("/Auth/PostLeadInfo", item);
            }
        }

        public static ApiResponse SaveLeadLoginInfo(LeadLoginInfo item)
        {
            lock (LeadInfoLock)
            {
                return Rest.MakePostRestCall<ApiResponse>("/Auth/PostLeadInfo", item);
            }
        }

        public static ApiResponse AddMettingScheduled(int LeadId, DateTime Date, TimeSpan TimeSlot, string Address)
        {
            lock (RegisterUserLock)
            {
                return Rest.MakePostRestCall<ApiResponse>("/Auth/AddMeeting/", new MeetingDetails(LeadId, Date, TimeSlot, Address));
            }
        }

        public static List<BankDetailItem> GetBanks()
        {
            lock (BanksLock)
            {
                return Rest.MakeGetRestCall<List<BankDetailItem>>("/User/GetAllBanks");
            }
        }
        public static ObservableCollection<MeetingItems> GetViewMeetings(DateTime Date, bool ShowAll)
        {
            lock (GetViewLeadsLock)
            {
                String url = "";
                if (ShowAll == true)
                {
                    url = "/Auth/GetLeadMeetings?Date=" + "";
                }
                else
                {
                    url = "/Auth/GetLeadMeetings?Date=" + Date.ToString("yyyy-MM-dd");
                }



                return Rest.MakeGetRestCall<ObservableCollection<MeetingItems>>(url);
            }
        }
    }
}
