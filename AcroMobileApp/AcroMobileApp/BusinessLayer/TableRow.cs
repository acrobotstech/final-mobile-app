﻿namespace acrobots.mobile.BusinessLayer
{
    //Generic value/text table row, currently used in the bills table
    public class TableRow
    {
        public TableRow(string firstTextItem, string value)
        {
            FirstTextItem = firstTextItem;
            Value = value;
        }

        public string FirstTextItem { get; set; }

        public string Value { get; set; }

        public string Heading { get; set; }
        public string BillTotal { get; set; }
    }
}