﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using acrobots.ResX;
using Xamarin.Forms;

namespace acrobots.mobile.BusinessLayer
{
    public static class Helper
    {
        private const decimal SecsInMin = 60;
        private const decimal Kb = 1024;
        private const decimal Mb = Kb * Kb;
        private const decimal Gb = Mb * Kb;
        public const string DensityLow = "low";
        public const string DensityMedium = "med";
        public const string DensityHigh = "high";
        public const string DensityXhigh = "xhigh";
        public const string DensityXxhigh = "xxhigh";
        public const string DensityXxxhigh = "xxxhigh";

        public static DateTime DateTimeFromJava(long javaTime)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(javaTime);
            return dtDateTime;
        }

        public static string ShortMonth(DateTime month)
        {
            switch (month.Month)
            {
                case 1:
                    return AppResources.JanuaryShort;
                case 2:
                    return AppResources.FebruaryShort;
                case 3:
                    return AppResources.MarchShort;
                case 4:
                    return AppResources.AprilShort;
                case 5:
                    return AppResources.MayShort;
                case 6:
                    return AppResources.JuneShort;
                case 7:
                    return AppResources.JulyShort;
                case 8:
                    return AppResources.AugustShort;
                case 9:
                    return AppResources.SeptemberShort;
                case 10:
                    return AppResources.OctoberShort;
                case 11:
                    return AppResources.NovemberShort;
                case 12:
                    return AppResources.DecemberShort;
                default:
                    return "";
            }
        }

        public static string FormatTimeBalance(decimal totalSeconds, bool showSecondsAndMinutes)
        {
            if (totalSeconds == 0)
            {
                return 0 + " " + AppResources.Minutes;
            }

            if (totalSeconds < 60)
            {
                return totalSeconds + " " + AppResources.Seconds;
            }

            var secondsComponentValue = (int) (totalSeconds % SecsInMin);
            var minutesComponentValue = Math.Floor(totalSeconds / SecsInMin);
            var minuteWord = (minutesComponentValue == 1 ? AppResources.Minute : AppResources.Minutes);
            var secondWord = (secondsComponentValue == 1 ? AppResources.Second : AppResources.Seconds);

            if (showSecondsAndMinutes)
            {
                return minutesComponentValue.ToString("#,#", CultureInfo.CurrentCulture) + " " + minuteWord + " " + (secondsComponentValue > 0 ? secondsComponentValue + " " + secondWord : "");
            }

            //We are not showing seconds in this case, so round to the nearest minute
            if (secondsComponentValue >= 30) minutesComponentValue++;

            return minutesComponentValue.ToString("#,#", CultureInfo.CurrentCulture) + " " + minuteWord;
        }

        public static string FormatBalance(decimal d, string type, string currency, string currencyBefore)
        {
            return FormatBalance(d, type, currency, currencyBefore, true);
        }

        public static string FormatBalance(decimal d, string type, string currency, string currencyBefore, bool showDetail)
        {
            switch (type)
            {
                case "TIME":
                    return FormatTimeBalance(d, showDetail);
                case "DATAVOLUME":
                    return GetDataAmount(d).ToString("0.##") + " " + GetDataLabel(d);
                case "QUANTITY":
                    if (d == 0)
                        return "0";
                    else
                       return d.ToString("#,#", CultureInfo.CurrentCulture);
                case "MONEY":
                    return FormatAmount(d, currency, currencyBefore);
                default:
                    return d.ToString(CultureInfo.CurrentCulture);
            }
        }

        private static bool IsGb(decimal d)
        {
            return d >= Gb;
        }

        private static bool IsMb(decimal d)
        {
            return d >= Mb && d < Gb;
        }

        private static decimal GetDataAmount(decimal d)
        {
            if (IsGb(d))
                return Math.Round(d / Gb, 2, MidpointRounding.AwayFromZero);
            if (IsMb(d))
                return Math.Round(d / Mb, 2, MidpointRounding.AwayFromZero);
            return Math.Round(d / Kb, 2, MidpointRounding.AwayFromZero);
        }

        private static string GetDataLabel(decimal d)
        {
            if (IsGb(d)) {
                return "GB";
            }

            return IsMb(d) ? "MB" : "KB";
        }

        /// <summary>
        /// Format decimal as currency string according to device locale,
        /// but without the currency symbol.
        /// </summary>
        /// <param name="amount">The amount for format</param>
        /// <param name="decimalThreshold">Optional absolute value above/below which decimal places will not be shown in order to reduce string length</param>
        /// <returns>Formatted amount without currency symbol</returns>
        public static string FormatAmountNoSymbol(decimal amount, decimal? decimalThreshold = null)
        {
            string retString;
            if (decimalThreshold != null && Math.Abs(amount) >= decimalThreshold)
            {
                // Above threshold, truncate the decimal places
                decimal truncAmount = Math.Floor(amount);
                retString = truncAmount.ToString("C0", CultureInfo.CurrentCulture);
            }
            else
            {
                retString = amount.ToString("C", CultureInfo.CurrentCulture);
            }


            // Remove currency symbol
            return retString.Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, "").Trim();
        }

        public static string FormatAmount(decimal amount, string currencySymbol, string symbolBefore, decimal? decimalThreshold = null)
        {
            string retString = FormatAmountNoSymbol(amount, decimalThreshold);

           

            if (symbolBefore.Equals("Y"))
                retString = currencySymbol + retString;
            else
                retString = retString + currencySymbol;
            return retString;
        }

        public static bool IsValidEmail(string email)
        {
            if (email == null)
            {
                return false;
            }

            //Setting regular expression same as SelfService.
            Regex regex = new Regex("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
            Match match = regex.Match(email);
            return match.Success;
        }

        public static bool IsValidAmountToPay(String amount)
        {
            Regex rgx = new Regex((@"^[0-9]\d{0,9}(\.\d{1,2})?%?$"));
            return rgx.IsMatch(amount);
        }

        public static DateTime GetResetDateTime()
        {
            return DateTime.MinValue; //retruns a time longer than 1min ago to reset refresh
        }

        public static ImageSource GetCardImage(string cardType)
        {
            switch (cardType)
            {
                case "VISA":
                    return ImageSource.FromFile("visaLogo.png");
                case "VISAD":
                    return ImageSource.FromFile("visaDLogo.png");
                case "AMEX":
                    return ImageSource.FromFile("amexLogo.png");
                case "MASTER":
                    return ImageSource.FromFile("masterLogo.png");
                case "MASTERD":
                    return ImageSource.FromFile("masterLogo.png");
                default:
                    return ImageSource.FromFile("genericCard.png");
            }
        }

        public static double GetDeviceScaledHeight()
        {
            // For Andriod devices, the scaled height returned by Device.Info.ScaledScreenSize.Height is the same as Device.Info.ScaledScreenSize.Width,
            // which is a bug logged with Xamarin: https://github.com/xamarin/Xamarin.Forms/issues/1579
            // Scaled height can also be achieved by dividing height in pixels by the scaling factor.
            return Device.Info.PixelScreenSize.Height / Device.Info.ScalingFactor;
        }

        public static DateTime AddSecondsToCurrentTime(long seconds)
        {
            return DateTime.Now.AddSeconds(seconds);
        }
    }
}