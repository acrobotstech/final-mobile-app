﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace AcroMobileApp.Controls
{
    public partial class PageHeaderWithCenterImageView : ContentView
    {
        public static readonly BindableProperty MainImageSourceProperty = BindableProperty.Create(nameof(MainImageSource), typeof(ImageSource), typeof(PageHeaderWithCenterImageView), null);

        public ImageSource MainImageSource
        {
            get { return (ImageSource)GetValue(MainImageSourceProperty); }
            set { SetValue(MainImageSourceProperty, value); }
        }

        public static readonly BindableProperty CenterImageSourceProperty = BindableProperty.Create(nameof(CenterImageSource), typeof(ImageSource), typeof(PageHeaderWithCenterImageView), null);

        public ImageSource CenterImageSource
        {
            get { return (ImageSource)GetValue(CenterImageSourceProperty); }
            set { SetValue(CenterImageSourceProperty, value); }
        }

        public static readonly BindableProperty BackButtonCommandProperty = BindableProperty.Create(nameof(BackButtonCommand), typeof(ICommand), typeof(PageHeaderWithCenterImageView), null);

        public ICommand BackButtonCommand
        {
            get { return (ICommand)GetValue(BackButtonCommandProperty); }
            set { SetValue(BackButtonCommandProperty, value); }
        }

        public static readonly BindableProperty BackButtonCommandParameterProperty = BindableProperty.Create(nameof(BackButtonCommandParameter), typeof(object), typeof(PageHeaderWithCenterImageView), null);

        public object BackButtonCommandParameter
        {
            get { return (object)GetValue(BackButtonCommandParameterProperty); }
            set { SetValue(BackButtonCommandParameterProperty, value); }
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(PageHeaderWithCenterImageView), string.Empty);

        public string HeaderText
        {
            get { return (string)GetValue(HeaderTextProperty); }
            set { SetValue(HeaderTextProperty, value); }
        }

        public static readonly BindableProperty IsBackButtonVisibleProperty = BindableProperty.Create(nameof(IsBackButtonVisible), typeof(bool), typeof(PageHeaderWithCenterImageView), true);

        public bool IsBackButtonVisible
        {
            get { return (bool)GetValue(IsBackButtonVisibleProperty); }
            set { SetValue(IsBackButtonVisibleProperty, value); }
        }

        public static readonly BindableProperty IsCenterImageVisibleProperty = BindableProperty.Create(nameof(IsCenterImageVisibleProperty), typeof(bool), typeof(PageHeaderWithCenterImageView), true);

        public bool IsCenterImageVisible
        {
            get { return (bool)GetValue(IsCenterImageVisibleProperty); }
            set { SetValue(IsCenterImageVisibleProperty, value); }
        }

        public PageHeaderWithCenterImageView()
        {
            InitializeComponent();
            BackButtonContainer.BindingContext = this;
            GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => BackButtonCommand?.Execute(BackButtonCommandParameter))
            });

        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if(MainImageContainer != null && propertyName == MainImageSourceProperty.PropertyName)
            {
                MainImageContainer.Source = MainImageSource;
            }

            if (CenterImageContainer != null && propertyName == CenterImageSourceProperty.PropertyName)
            {
                CenterImageContainer.Source = CenterImageSource;
            }

            if (HeaderTextContainer != null && propertyName == HeaderTextProperty.PropertyName)
            {
                HeaderTextContainer.Text = HeaderText;
            }

            if(BackButtonContainer != null && propertyName == IsBackButtonVisibleProperty.PropertyName)
            {
                BackButtonContainer.IsVisible = IsBackButtonVisible;
            }
        }
    }
}
