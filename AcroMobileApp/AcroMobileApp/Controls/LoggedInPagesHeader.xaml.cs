﻿using AcroMobileApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoggedInPagesHeader : ContentView
	{

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(CustomEntryView), string.Empty, BindingMode.TwoWay, null, OnTextPropertyChanged, null, null, null);

        public string HeaderText
        {
            get { return (string)GetValue(HeaderTextProperty); }
            set { SetValue(HeaderTextProperty, value); }
        }


        public LoggedInPagesHeader ()
		{
			InitializeComponent ();
            HeaderTextContainer.BindingContext = this;
        }

        async void OnBackImageButtonClicked(object sender, EventArgs e)
        {

            await Navigation.PopAsync();

        }

        public static void OnTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as LoggedInPagesHeader;
            if (control == null) return;
            if (newValue != null)
                control.HeaderText = newValue.ToString();
        }
    }
}