﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace AcroMobileApp.Controls
{
    public partial class CustomComboBoxView : ContentView
    {
        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            SelectedItemChangedCommand?.Execute(SelectedItemChangedCommandParameter);
        }

        public Func<string, ICollection<string>, ICollection<string>> SortingAlgorithm { get; } = (text, values) => values
        .Where(x => x.ToLower().Contains(text.ToLower()))
        .OrderBy(x => x)
        .ToList();

        public CustomComboBoxView()
        {
            InitializeComponent();
            PickerContainer.BindingContext = this;
            if(SelectedItemChangedCommand != null)
                SelectedItemChangedCommand = new Command (() => SelectedItemChangedCommand?.Execute(SelectedItemChangedCommandParameter));
        }

        public static readonly BindableProperty ComboBoxItemsSourceProperty = BindableProperty.Create(nameof(ComboBoxItemsSource), typeof(ObservableCollection<string>), typeof(CustomComboBoxView), null);

        public ObservableCollection<string> ComboBoxItemsSource
        {
            get { return (ObservableCollection<string>)GetValue(ComboBoxItemsSourceProperty); }
            set { SetValue(ComboBoxItemsSourceProperty, value); }
        }

        public static readonly BindableProperty ComboBoxPlaceholderProperty = BindableProperty.Create(nameof(ComboBoxPlaceholder), typeof(string), typeof(CustomComboBoxView), string.Empty);

        public string ComboBoxPlaceholder
        {
            get { return (string)GetValue(ComboBoxPlaceholderProperty); }
            set { SetValue(ComboBoxPlaceholderProperty, value); }
        }

        public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create(nameof(ImageSource), typeof(ImageSource), typeof(CustomComboBoxView), null);

        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly BindableProperty SelectedItemChangedCommandProperty = BindableProperty.Create(nameof(SelectedItemChangedCommand), typeof(ICommand), typeof(CustomComboBoxView), null);

        public ICommand SelectedItemChangedCommand
        {
            get { return (ICommand)GetValue(SelectedItemChangedCommandProperty); }
            set { SetValue(SelectedItemChangedCommandProperty, value); }
        }

        public static readonly BindableProperty SelectedItemChangedCommandParameterProperty = BindableProperty.Create(nameof(SelectedItemChangedCommandParameter), typeof(object), typeof(CustomComboBoxView), null);

        public object SelectedItemChangedCommandParameter
        {
            get { return (object)GetValue(SelectedItemChangedCommandParameterProperty); }
            set { SetValue(SelectedItemChangedCommandParameterProperty, value); }
        }

        public static readonly BindableProperty ComboBoxSelectedItemProperty = BindableProperty.Create(nameof(ComboBoxSelectedItem), typeof(object), typeof(CustomComboBoxView), null, BindingMode.TwoWay, null, OnSelectedItemPropertyChanged);

        public object ComboBoxSelectedItem
        {
            get { return (object)GetValue(ComboBoxSelectedItemProperty); }
            set { SetValue(ComboBoxSelectedItemProperty, value); }
        }

        public static readonly BindableProperty ComboBoxTextProperty = BindableProperty.Create(nameof(ComboBoxText), typeof(string), typeof(CustomComboBoxView), string.Empty, BindingMode.TwoWay);

        public string ComboBoxText
        {
            get { return (string)GetValue(ComboBoxTextProperty); }
            set { SetValue(ComboBoxTextProperty, value); }
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if(PickerContainer != null && propertyName == ComboBoxPlaceholderProperty.PropertyName)
            {
                //PickerContainer.Placeholder = ComboBoxPlaceholder;
                PickerContainer.Title = ComboBoxPlaceholder;                
            }

            if (PickerContainer != null && propertyName == ComboBoxItemsSourceProperty.PropertyName)
            {
                PickerContainer.ItemsSource = ComboBoxItemsSource;
              


            }

            if (PickerContainer != null && propertyName == ComboBoxTextProperty.PropertyName)
            {
               
            }

            if (ImageContainer != null && propertyName == ImageSourceProperty.PropertyName)
            {
                ImageContainer.Source = ImageSource;
                ImageContainer.IsVisible = true;
            }
        }

        public static void OnSelectedItemPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as CustomComboBoxView;
            if (control == null) return;
            if (newValue != null)
            {
                control.PickerContainer.SelectedItem = newValue;
                
            }
        }

        private void PickerContainer_SelectedIndexChanged(object sender, EventArgs e)
        {
            PickerContainer.SelectedItem = ((Picker)sender).SelectedItem;
            PlaceholderContainer.IsVisible = true;
            SelectedItemChangedCommand?.Execute(SelectedItemChangedCommandParameter);
            PlaceholderContainer.Text = ComboBoxPlaceholder;
        }
    }
}
