﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomEntryView : ContentView
    {

        public static readonly BindableProperty KeyboardTypeProperty = BindableProperty.Create(nameof(KeyboardType), typeof(string), typeof(CustomEntryView), string.Empty, BindingMode.TwoWay, null, OnTextPropertyChanged, null, null, null);

        public String KeyboardType
        {
            get { return (String)GetValue(KeyboardTypeProperty); }
            set { SetValue(KeyboardTypeProperty, value); }
        }

        public static readonly BindableProperty IsMandatoryProperty = BindableProperty.Create(nameof(IsMandatory), typeof(bool), typeof(CustomEntryView), false);

        public bool IsMandatory
        {
            get { return (bool)GetValue(IsMandatoryProperty); }
            set { SetValue(IsMandatoryProperty, value); }
        }

        public static readonly BindableProperty EntryTextProperty = BindableProperty.Create(nameof(EntryText), typeof(string), typeof(CustomEntryView),string.Empty, BindingMode.TwoWay, null, OnTextPropertyChanged, null, null, null);

        public string EntryText
        {
            get { return (string)GetValue(EntryTextProperty); }
            set { SetValue(EntryTextProperty, value); }
        }

        public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create(nameof(ImageSource), typeof(ImageSource), typeof(CustomEntryView), null);

        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly BindableProperty EntryPlaceholderProperty = BindableProperty.Create(nameof(EntryPlaceholder), typeof(string), typeof(CustomEntryView), string.Empty);

        public string EntryPlaceholder
        {
            get { return (string)GetValue(EntryPlaceholderProperty); }
            set { SetValue(EntryPlaceholderProperty, value); }
        }

        public static readonly BindableProperty EntryErrorTextProperty = BindableProperty.Create(nameof(EntryErrorText), typeof(string), typeof(CustomEntryView), string.Empty, BindingMode.TwoWay);

        public string EntryErrorText
        {
            get { return (string)GetValue(EntryErrorTextProperty); }
            set { SetValue(EntryErrorTextProperty, value); }
        }

        public CustomEntryView()
        {
            InitializeComponent();
            EntryContainer.BindingContext = this;
            EntryContainer.ActivePlaceholderColor = Color.FromHex("#0006b0");
            EntryContainer.PlaceholderColor = Color.FromHex("#0006b0");
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (EntryContainer != null && propertyName == EntryTextProperty.PropertyName)
            {
                EntryContainer.Text = EntryText;
            }

            if (EntryContainer != null && propertyName == EntryPlaceholderProperty.PropertyName)
            {
                EntryContainer.Placeholder = EntryPlaceholder;
                


            }

            if (ImageContainer != null && propertyName == ImageSourceProperty.PropertyName)
            {
                ImageContainer.Source = ImageSource;
                ImageContainer.IsVisible = true;
            }

            if (EntryContainer != null && propertyName == EntryErrorTextProperty.PropertyName)
            {
                EntryContainer.ErrorText = EntryErrorText;
            }

            if (EntryContainer != null && KeyboardType != null && KeyboardType !="" && propertyName == KeyboardTypeProperty.PropertyName)
            {
                if (KeyboardType == "Numeric")
                    EntryContainer.Keyboard = Keyboard.Numeric;
            }
        }

       

        public static void OnTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as CustomEntryView;
            if (control == null) return;
            if (newValue != null)
                control.EntryContainer.Text = newValue.ToString();
        }
    }
}
