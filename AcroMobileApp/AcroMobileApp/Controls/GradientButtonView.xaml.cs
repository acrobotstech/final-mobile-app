﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace AcroMobileApp.Controls
{
    public partial class GradientButtonView : ContentView
    {
		public static readonly BindableProperty ButtonCommandProperty = BindableProperty.Create(nameof(ButtonCommand), typeof(ICommand), typeof(GradientButtonView), null);

        public ICommand ButtonCommand
        {
            get { return (ICommand)GetValue(ButtonCommandProperty); }
            set { SetValue(ButtonCommandProperty, value); }
        }

		public static readonly BindableProperty ButtonCommandParameterProperty = BindableProperty.Create(nameof(ButtonCommandParameter), typeof(object), typeof(GradientButtonView), null);

        public object ButtonCommandParameter
        {
            get { return (object)GetValue(ButtonCommandParameterProperty); }
            set { SetValue(ButtonCommandParameterProperty, value); }
        }

		public static readonly BindableProperty ButtonTextProperty = BindableProperty.Create(nameof(ButtonText), typeof(string), typeof(GradientButtonView), string.Empty);

        public string ButtonText
        {
            get { return (string)GetValue(ButtonTextProperty); }
            set { SetValue(ButtonTextProperty, value); }
        }

        public static readonly BindableProperty ButtonImageSourceProperty = BindableProperty.Create(nameof(ButtonImageSource), typeof(string), typeof(GradientButtonView), "box_btn.png");

        public string ButtonImageSource
        {
            get { return (string)GetValue(ButtonImageSourceProperty); }
            set { SetValue(ButtonImageSourceProperty, value); }
        }

        public static readonly BindableProperty ButtonTextColorProperty = BindableProperty.Create(nameof(ButtonTextColor), typeof(Color), typeof(GradientButtonView),Color.White);

        public Color ButtonTextColor
        {
            get { return (Color)GetValue(ButtonTextColorProperty); }
            set { SetValue(ButtonTextColorProperty, value); }
        }

        public GradientButtonView()
        {
            ButtonTextColor = Color.White;
            ButtonImageSource = "box_btn.png";
            InitializeComponent();
			GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => ButtonCommand?.Execute(ButtonCommandParameter))
            });
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
			base.OnPropertyChanged(propertyName);
            
			if (LabelContainer != null && propertyName == ButtonTextProperty.PropertyName)
            {
				LabelContainer.Text = ButtonText;
            }

            if (ImageContainer != null && propertyName == ButtonImageSourceProperty.PropertyName)
            {
                ImageContainer.Source = ButtonImageSource;
            }

            if (LabelContainer != null && propertyName == ButtonTextColorProperty.PropertyName)
            {
                LabelContainer.TextColor = ButtonTextColor;
            }
        }
    }
}
