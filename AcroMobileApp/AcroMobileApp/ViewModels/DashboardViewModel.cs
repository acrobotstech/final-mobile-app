﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using acrobots.mobile;
using AcroMobileApp.BusinessLayer;
using AcroMobileApp.BusinessLayer.Model;
using AcroMobileApp.Views;
using Xamarin.Forms;

namespace AcroMobileApp.ViewModels
{
	public class DashboardViewModel : BaseViewModel
    {
        private int inProgress;
        public int InProgress
        {
            get { return inProgress; }
            set { inProgress = value; OnPropertyChanged(); }
        }

        private int approved;
        public int Approved
        {
            get { return approved; }
            set { approved = value; OnPropertyChanged(); }
        }

        private int disbursed;
        public int Disbursed
        {
            get { return disbursed; }
            set { disbursed = value; OnPropertyChanged(); }
        }

        private int rejected;
        public int Rejected
        {
            get { return rejected; }
            set { rejected = value; OnPropertyChanged(); }
        }

        private int totalEarning;
        public int TotalEarning
        {
            get { return totalEarning; }
            set { totalEarning = value; OnPropertyChanged(); }
        }
        
        public DashboardViewModel()
        {
            
        }

        public async Task FetchAllData()
        {
            await Task.Run(() => 
            {
                IsBusy = true;
                GetPerformanceOverview();
                IsBusy = false;
            });
        }

        private void GetPerformanceOverview()
        {
            try { 
            UserPerformanceItem userPerformanceItem = BusinessManager.GetUserPerformance();

            InProgress = userPerformanceItem.InProgress;
            Approved = userPerformanceItem.Aprroved;
            Disbursed = userPerformanceItem.Disbursed;
            Rejected = userPerformanceItem.Rejected;
            TotalEarning = userPerformanceItem.TotalEarning;
            }
            catch (ApiCallFailedException ex)
            {
                //Seems no internet connection transfer user to no internet connection page
                if(ex.ResponseCode == 2)
                App.Current.MainPage = new NavigationPage(new NoInternetConnection());
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                return;
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}