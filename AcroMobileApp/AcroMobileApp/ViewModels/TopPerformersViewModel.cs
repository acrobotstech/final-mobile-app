﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AcroMobileApp.Models;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using System.Windows.Input;
using AcroMobileApp.BusinessLayer.Model;
using System.Collections.Generic;
using acrobots.mobile;

namespace AcroMobileApp.ViewModels
{
    class TopPerformersViewModel : BaseViewModel
    {
        
        public Command RefreshListCommand { get; set; }
        ObservableCollection<TopPerformersItem> items = new ObservableCollection<TopPerformersItem>();

       
        public ObservableCollection<TopPerformersItem> Items
        {
            get { return items; }
            set { items = value; OnPropertyChanged(); }
        }


        public TopPerformersViewModel()
        {
            
            
            RefreshListCommand = new Command(async () => await refreshItems());
            Device.BeginInvokeOnMainThread(() =>
            {
                if (IsBusy)
                    return;

                IsBusy = true;
                try
                {

                   
                    Items = BusinessManager.GetTopPerformers();


                    IsBusy = false;
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }
            });
            }

        public async Task refreshItems()
        {
           
                if (IsBusy)
                    return;

                IsBusy = true;

                try
                {
             
                    await Task.Run(() =>
                    Items = BusinessManager.GetTopPerformers());
                   
                
                IsBusy = false;
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }
            
        }

       

    }

}
