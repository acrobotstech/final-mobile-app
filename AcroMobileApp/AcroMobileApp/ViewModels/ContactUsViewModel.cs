﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AcroMobileApp.Models;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using System.Windows.Input;
using AcroMobileApp.BusinessLayer.Model;
using System.Collections.Generic;
using acrobots.mobile;

namespace AcroMobileApp.ViewModels
{
   
    public class ContactUsViewModel
    {
        public ICommand BackButtonCommand { get; }
        INavigation navigation;
        public ContactUsViewModel(INavigation navigationRef)
        {
            BackButtonCommand = new Command(async () => await navigationRef.PushAsync(new MainPage()));
        }
    }
}
