﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AcroMobileApp.Models;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using System.Windows.Input;
using AcroMobileApp.BusinessLayer.Model;
using acrobots.mobile;

namespace AcroMobileApp.ViewModels
{
    class RaiseMeetingViewModel : BaseViewModel
    {





        INavigation navigation;

        private int leadId;
        public int LeadId
        {
            get { return leadId; }
            set { leadId = value; OnPropertyChanged(); }
        }
        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { date = value; OnPropertyChanged(); }
        }

        private TimeSpan timeSlot;
        public TimeSpan TimeSlot
        {
            get { return timeSlot; }
            set { timeSlot = value; OnPropertyChanged(); }
        }

        private string address;
        public string Address
        {
            get { return address; }
            set { address = value; OnPropertyChanged(); }
        }

        public ICommand SaveMeetingDetailsCommand { get; }
        public ICommand BackButtonCommand { get; }

        public ICommand LoginNowCommand { get; }



        public RaiseMeetingViewModel(INavigation navigationRef, int leadId)
        {
            LeadId = leadId;
            navigation = navigationRef;
            SaveMeetingDetailsCommand = new Command(async () => await SaveMeetingDetails());
            BackButtonCommand = new Command(async () => await navigation.PopAsync());

        }

        async Task SaveMeetingDetails()
        {
            if (IsInputValid())
            {
                await Task.Run(() =>
                {
                    IsBusy = true;
                    try
                    {
                        ApiResponse response = BusinessManager.AddMettingScheduled(LeadId, Date, TimeSlot, Address);
                        IsBusy = false;
                        if (response.responseCode == 0)
                        {

                            Device.BeginInvokeOnMainThread(() => navigation.PopAsync());
                            Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(response.messages.messageDescription));
                            return;
                        }
                    }
                    catch (ApiCallFailedException ex)
                    {
                        Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                        return;
                    }
                    finally
                    {
                        IsBusy = false;
                    }

                });
            }
        }



        private bool IsInputValid()
        {
            if (Date == null)
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Select Date");
                return false;
            }

            if (TimeSlot == null)
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("select Time");
                return false;
            }



            if (Address == null)
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter Address");
                return false;
            }
            return true;
        }



    }

}
