﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using acrobots.mobile.Web;
using AcroMobileApp.BusinessLayer;
using AcroMobileApp.BusinessLayer.Model;
using AcroMobileApp.Views;
using Xamarin.Forms;

namespace AcroMobileApp.ViewModels
{
    public class FileDetailsViewModel : BaseViewModel
    {
        INavigation navigation;
        private string documentName;
        public string DocumentName
        {
            get { return documentName; }
            set { documentName = value; OnPropertyChanged(); }
        }

        private string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; OnPropertyChanged(); }
        }

        private string fileURL;
        public string FileURL
        {
            get { return fileURL; }
            set { fileURL = value; OnPropertyChanged(); }
        }

        private bool isFilePresent;
        public bool IsFilePresent
        {
            get { return isFilePresent; }
            set { isFilePresent = value; OnPropertyChanged(); }
        }

        public ICommand AttachFileCommand { get; }

        public ICommand ViewFileCommand { get; }

        public ICommand DeleteFileCommand { get; }

        public FileDetailsViewModel(INavigation navigationRef, DocumentDetails item)
        {
            this.navigation = navigationRef;

            DocumentName = item.DocumentName;

            FileName = item.FileName;

            FileURL = item.FileFullUrl;

            IsFilePresent = !string.IsNullOrEmpty(FileURL);

            AttachFileCommand = new Command(async () => await AttachFile());

            ViewFileCommand = new Command(async () => await ViewFile());

            DeleteFileCommand = new Command(async () => await DeleteFile());
        }

        private async Task AttachFile()
        {
            
            var file = await Task<Plugin.FilePicker.Abstractions.FileData>.Run(() =>
            {
                return Plugin.FilePicker.CrossFilePicker.Current.PickFile();
            });


        }

        private async Task ViewFile()
        {
            await Task.Run(() =>
            {
                string fullFilePath = "http://api.apnarupee.in/api/" + FileURL;
                Device.BeginInvokeOnMainThread(() => navigation.PushAsync(new ViewDocumentPage(navigation, fullFilePath)));
            });
        }

        private async Task DeleteFile()
        {
            await Task.Run(() =>
            {

            });
        }
    }
}
