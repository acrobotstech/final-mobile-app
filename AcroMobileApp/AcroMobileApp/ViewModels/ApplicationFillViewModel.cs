﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using acrobots.mobile;
using AcroMobileApp.BusinessLayer;
using AcroMobileApp.BusinessLayer.Model;
using Xamarin.Forms;

namespace AcroMobileApp.ViewModels
{
	public class ApplicationFillViewModel : BaseViewModel
    {
        private int leadId;
        LeadInfoItem item;
        INavigation navigation;

        private enum Tabs
        {
            LeadInfo = 0,
            PersonalInfo = 1,
            LoginInfo = 2
        }
        private const string LeadInfo = "LeadInfo";
        private const string PersonalInfo = "PersonalInfo";
        private const string LoginInfo = "LoginInfo";

        private bool isLeadInfoTabSelected;
        public bool IsLeadInfoTabSelected
        {
            get { return isLeadInfoTabSelected; }
            set { isLeadInfoTabSelected = value; OnPropertyChanged(); }
        }

        private bool isPersonalInfoTabSelected;
        public bool IsPersonalInfoTabSelected
        {
            get { return isPersonalInfoTabSelected; }
            set { isPersonalInfoTabSelected = value; OnPropertyChanged(); }
        }

        private bool isLoginInfoTabSelected;
        public bool IsLoginInfoTabSelected
        {
            get { return isLoginInfoTabSelected; }
            set { isLoginInfoTabSelected = value; OnPropertyChanged(); }
        }

        private string fullName;
        public string FullName
        {
            get { return fullName; }
            set { fullName = value; OnPropertyChanged(); }
        }

        private string mobileNo;
        public string MobileNo
        {
            get { return mobileNo; }
            set { mobileNo = value; OnPropertyChanged(); }
        }

        private string emailId;
        public string EmailId
        {
            get { return emailId; }
            set { emailId = value; OnPropertyChanged(); }
        }

        private string city;
        public string City
        {
            get { return city; }
            set { city = value; OnPropertyChanged(); }
        }

        private double loanAmount;
        public double LoanAmount
        {
            get { return loanAmount; }
            set { loanAmount = value; OnPropertyChanged(); }
        }

        private string tenure;
        public string Tenure
        {
            get { return tenure; }
            set { tenure = value; OnPropertyChanged(); }
        }

        private DateTime dob;
        public DateTime DOB
        {
            get { return dob; }
            set { dob = value; OnPropertyChanged(); }
        }

        private string selectedGender;
        public string SelectedGender
        {
            get { return selectedGender; }
            set { selectedGender = value; OnPropertyChanged(); }
        }

        private string selectedGenderText;
        public string SelectedGenderText
        {
            get { return selectedGenderText; }
            set { selectedGenderText = value; OnPropertyChanged(); }
        }

        private string panCard;
        public string PANCard
        {
            get { return panCard; }
            set { panCard = value; OnPropertyChanged(); }
        }

        private string aadhaarCard;
        public string AadhaarCard
        {
            get { return aadhaarCard; }
            set { aadhaarCard = value; OnPropertyChanged(); }
        }

        private string address;
        public string Address
        {
            get { return address; }
            set { address = value; OnPropertyChanged(); }
        }

        private string pinCode;
        public string PinCode
        {
            get { return pinCode; }
            set { pinCode = value; OnPropertyChanged(); }
        }

        private string fileLoginBank;
        public string FileLoginBank
        {
            get { return fileLoginBank; }
            set { fileLoginBank = value; OnPropertyChanged(); }
        }

        private string fileLoginDate;
        public string FileLoginDate
        {
            get { return fileLoginDate; }
            set { fileLoginDate = value; OnPropertyChanged(); }
        }

        private string bankApplicationID;
        public string BankApplicationID
        {
            get { return bankApplicationID; }
            set { bankApplicationID = value; OnPropertyChanged(); }
        }

        private string bankRMName;
        public string BankRMName
        {
            get { return bankRMName; }
            set { bankRMName = value; OnPropertyChanged(); }
        }

        private long bankRMPhoneNumber;
        public long BankRMPhoneNumber
        {
            get { return bankRMPhoneNumber; }
            set { bankRMPhoneNumber = value; OnPropertyChanged(); }
        }

        private string bankRMEmail;
        public string BankRMEmail
        {
            get { return bankRMEmail; }
            set { bankRMEmail = value; OnPropertyChanged(); }
        }

        private string bankBranch;
        public string BankBranch
        {
            get { return bankBranch; }
            set { bankBranch = value; OnPropertyChanged(); }
        }

        private string documentStatus;
        public string DocumentStatus
        {
            get { return documentStatus; }
            set { documentStatus = value; OnPropertyChanged(); }
        }

        private int selectedTab = 0;
        public int SelectedTab
        {
            get { return selectedTab; }
            set { selectedTab = value; OnPropertyChanged(); }
        }

        private ObservableCollection<string> gender = new ObservableCollection<string>{"Male", "Female"};
        public ObservableCollection<string> Gender
        {
            get { return gender; }
            set { gender = value; OnPropertyChanged(); }
        }

        public ICommand TabIndexChangedCommand { get; }

        public ICommand ContentSwipeCommand { get; }

        public ICommand BackButtonCommand { get; }

        public ICommand SaveLeadInfoCommand { get; }
        public ICommand SavePersonalInfoCommand { get; }
        public ICommand SaveLoginInfoCommand { get; }

        public ApplicationFillViewModel(INavigation navigationRef, int leadID)
        {
            navigation = navigationRef;
            leadId = leadID;
            IsLeadInfoTabSelected = true;

            TabIndexChangedCommand = new Command((parameter) => TabIndexChanged(parameter));

            ContentSwipeCommand = new Command((parameter) => ContentSwipe(parameter));

            SaveLeadInfoCommand = new Command(async () => await SaveDetails(LeadInfo));

            SavePersonalInfoCommand = new Command(async () => await SaveDetails(PersonalInfo));

            SaveLoginInfoCommand = new Command(async () => await SaveDetails(LoginInfo));

            BackButtonCommand = new Command(async () => await navigation.PopAsync());
        }

        public async Task FetchAllData()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                GetLeadInfo();
                IsBusy = false;
            });
        }

        private void GetLeadInfo()
        {
            
            item = BusinessManager.GetLeadInfo(leadId);

            if (item.ContactInfo.Count > 0 && item.ContactInfo[0] != null)
            {
                City = item.ContactInfo[0].City;
                EmailId = item.ContactInfo[0].EmailId;
                LoanAmount = item.ContactInfo[0].LoanAmount;
                MobileNo = item.ContactInfo[0].MobileNo;
                FullName = item.ContactInfo[0].FullName;
                Tenure = item.ContactInfo[0].TenureInMonths.ToString();
            }

            if (item.PersonalInfo.Count > 0 &&  item.PersonalInfo[0] != null)
            {
                AadhaarCard = item.PersonalInfo[0].AadharNo;
                Address = item.PersonalInfo[0].Address;
                DOB = item.PersonalInfo[0].DOB;
                SelectedGender = item.PersonalInfo[0].Gender;
                PANCard = item.PersonalInfo[0].PancardNo;
                PinCode = item.PersonalInfo[0].PinCode;


            }
            //Bank = item.LoginInfo[0].BankId;

            if (item.LoginInfo.Count > 0 && item.PersonalInfo[0] != null)
            {
                BankApplicationID = item.LoginInfo[0].BankApplicationId;
                BankBranch = item.LoginInfo[0].BankBranch;
                BankRMEmail = item.LoginInfo[0].BankRMEmail;
                BankRMPhoneNumber = item.LoginInfo[0].BankRMMobile;
                BankRMName = item.LoginInfo[0].BankRMName;
                DocumentStatus = item.LoginInfo[0].DocPickupStatus;
                FileLoginDate = item.LoginInfo[0].FileLoginDate;
            }
        }

        private void TabIndexChanged(object parameter)
        {
            int moveToTabNo = Convert.ToInt16(parameter);

            ChangeSelectedTab(moveToTabNo);

            SelectedTab = moveToTabNo;
        }

        private void ContentSwipe(object parameter)
        {
            string swipeDirection = parameter as string;

            if (swipeDirection == "Left")
                SelectedTab++;
            else
                SelectedTab--;

            ChangeSelectedTab(SelectedTab);
        }

        private void ChangeSelectedTab(int moveToTabNo)
        {
            IsLeadInfoTabSelected = false;
            IsPersonalInfoTabSelected = false;
            IsLoginInfoTabSelected = false;

            switch (moveToTabNo)
            {
                case (int)Tabs.LeadInfo:
                    IsLeadInfoTabSelected = true;
                    break;
                case (int)Tabs.PersonalInfo:
                    IsPersonalInfoTabSelected = true;
                    break;
                case (int)Tabs.LoginInfo:
                    IsLoginInfoTabSelected = true;
                    break;
                default:
                    moveToTabNo = SelectedTab;
                    break;
            }
        }

        private async Task SaveDetails(string requestType)
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                List<BankDetailItem> banks = BusinessManager.GetBanks();

                ApiResponse response = null;

                try
                {
                    if (requestType == LeadInfo)
                    {
                        LeadContactInfo info = new LeadContactInfo(leadId, "ContactInfo", FullName, MobileNo.ToString(), EmailId, City, LoanAmount, Convert.ToInt32(Tenure));
                        response = BusinessManager.SaveLeadContactInfo(info);

                    }
                    else if (requestType == PersonalInfo)
                    {
                        LeadPersonalInfo info = new LeadPersonalInfo(leadId, "PersonalInfo", DOB, SelectedGender, PANCard, AadhaarCard, Address, PinCode);
                        response = BusinessManager.SaveLeadPersonalInfo(info);
                    }
                    else
                    {
                        LeadLoginInfo info = new LeadLoginInfo(leadId, "LoginInfo", BankRMName, FileLoginDate, BankApplicationID, BankRMPhoneNumber, "abc", "abc", "3", BankBranch, DocumentStatus);
                        response = BusinessManager.SaveLeadLoginInfo(info);
                    }
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }

                if (response.responseCode == 0)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage("Changes saved successfully"));
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(response.messages.messageDescription));
                }



                IsBusy = false;
            });
        }
    }
}
