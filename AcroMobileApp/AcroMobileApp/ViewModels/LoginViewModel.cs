﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using acrobots.mobile;
using AcroMobileApp.BusinessLayer;
using AcroMobileApp.BusinessLayer.Model;
using AcroMobileApp.Views;
using Xamarin.Forms;

namespace AcroMobileApp.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        INavigation navigationRef;

        private string mobileNumber;
        public string MobileNumber
        {
            get
            {
                return mobileNumber;
            }
            set
            {
                mobileNumber = value; OnPropertyChanged();
            }
        }

        public ICommand GetOtpCommand
        {
            get;
        }

        public ICommand RegisterUserCommand
        {
            get;
        }

        public LoginViewModel(INavigation navigation)
        {
            navigationRef = navigation;
            RegisterUserCommand = new Command(async () => await RegisterUser());
            GetOtpCommand = new Command(async () => await GetOtp());
        }

        async Task RegisterUser()
        {
            await navigationRef.PushAsync(new RegisterUserPage());
        }

        async Task GetOtp()
        {
            long enteredMobileNo;
            if (string.IsNullOrEmpty(MobileNumber))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter mobile number");
                return;
            }

            if (MobileNumber.Length != 10)
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Mobile number is 10 digits long");
                return;
            }

            if (!long.TryParse(MobileNumber, out enteredMobileNo))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Invalid mobile number");
                return;
            }

            await Task.Run(() =>
            {
                IsBusy = true;
                try
                {
                    ApiResponse response = BusinessManager.GetOtpForLogin(MobileNumber);
                    IsBusy = false;
                    if (response.responseCode == 0)
                    {
                        Device.BeginInvokeOnMainThread(() => navigationRef.PushAsync(new LoginWithOtpPage(navigationRef, MobileNumber)));
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(response.messages.messageDescription));
                    }
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally{
                    IsBusy = false;
                }
            });

        }
    }
}
