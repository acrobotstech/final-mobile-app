﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AcroMobileApp.Models;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using System.Windows.Input;
using acrobots.mobile;
using AcroMobileApp.BusinessLayer.Model;
using System.Collections.Generic;

namespace AcroMobileApp.ViewModels
{
    class ViewLeadsDocModel : BaseViewModel
    {
        public ObservableCollection<object> DisplayItems { get; set; } = new ObservableCollection<object>();

        private Dictionary<string, DocumentDetailsItem> docDetailsDictionary { get; set; } = new Dictionary<string, DocumentDetailsItem>();
        public ICommand BackButtonCommand { get; }

        INavigation navigation;
        int LeadId;
        public ViewLeadsDocModel(INavigation navigationRef, int leadId)
        {
            this.navigation = navigationRef;
            this.LeadId = leadId;
            BackButtonCommand = new Command(async () => await navigation.PopAsync());
            GetLeadDocDetails();
        }


        public async Task GetLeadDocDetail()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                Device.BeginInvokeOnMainThread(() => GetLeadDocDetails());
                IsBusy = false;
            });
        }

        private void GetLeadDocDetails()
        {
            try
            {
                DocumentDetailsResponse response = BusinessManager.GetLeadDocumentDetails(10005);

                if (response.DocumentDetails != null)
                {
                    for (int i = 0; i < response.DocumentDetails.Count; i++)
                    {
                        String categoryName = response.DocumentDetails[i].CategoryName;
                        if (docDetailsDictionary.ContainsKey(categoryName))
                        {
                            docDetailsDictionary[categoryName].addDocument(response.DocumentDetails[i]);

                        }
                        else
                        {
                            DocumentDetailsItem item = new DocumentDetailsItem(categoryName);
                            item.addDocument(response.DocumentDetails[i]);
                            docDetailsDictionary.Add(categoryName, item);
                        }
                    }

                    foreach (KeyValuePair<string, DocumentDetailsItem> entry in docDetailsDictionary)
                    {
                        DisplayItems.Add(new FileCategoryViewModel(entry.Key));
                        if (entry.Value != null && entry.Value.DocumentDetails.Count > 0)
                        {
                            for (int j = 0; j < entry.Value.DocumentDetails.Count; j++)
                            {
                                DisplayItems.Add(new FileDetailsViewModel(navigation, entry.Value.DocumentDetails[j]));
                                DisplayItems.Add(new EmptyCellViewModel());
                            }

                            DisplayItems.Add(new SeparatorCellViewModel());
                        }
                    }
                }
            }
            catch (ApiCallFailedException ex)
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                return;
            }
        }
    }
}
