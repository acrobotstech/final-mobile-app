﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AcroMobileApp.Models;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using System.Windows.Input;
using AcroMobileApp.BusinessLayer.Model;
using System.Collections.Generic;
using acrobots.mobile;

namespace AcroMobileApp.ViewModels
{
    class MeetingsViewModel : BaseViewModel
    {
        public ObservableCollection<MeetingItems> Items { get; set; }
        public Command RefreshListCommand { get; set; }
        public Command LoadItemsCommand { get; set; }
        public Command MakeCallCommand { get; set; }
        public ICommand GetMeetingDetailsCommand { get; }
        public ICommand BackButtonCommand { get; }
        INavigation navigation;
        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { date = value; OnPropertyChanged(); }
        }

        bool dataPresent = false;
        public bool DataPresent
        {
            get { return dataPresent; }
            set { dataPresent = value; OnPropertyChanged(); }
        }

        bool dataNotPresent = false;
        public bool DataNotPresent
        {
            get { return dataNotPresent; }
            set { dataNotPresent = value; OnPropertyChanged(); }
        }


        public MeetingsViewModel(INavigation navigationRef)
        {
            navigation = navigationRef;
            BackButtonCommand = new Command(async () => await navigationRef.PushAsync(new MainPage()));
            MakeCallCommand = new Command(async (parameter) => await MakeCall(parameter));
            Items = new ObservableCollection<MeetingItems>();
            RefreshListCommand = new Command(async () => await refreshItems());

            date = DateTime.Now;

            Device.BeginInvokeOnMainThread(() =>
            {
                refreshItems();

            });
            }

        public async Task refreshItems()
        {
           
                if (IsBusy)
                    return;

                IsBusy = true;

                try
                {
                ObservableCollection<MeetingItems> pagedData = null;
                    await Task.Run(() =>
              pagedData = BusinessManager.GetViewMeetings(date, true));
                   

                    for (int i = 0; i < pagedData.Count; i++)
                    {

                        Items.Add(pagedData[i]);
                    }

                DataPresent = Items.Count > 0;
                DataNotPresent = !DataPresent;
                IsBusy = false;
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }
            
        }

        public int getIndexOf(MeetingItems item)
        {
            return Items.IndexOf(item);
        }

        public async Task ReloadItems()
        {

            if (IsBusy)
                return;

            IsBusy = true;

            try
            {

                ObservableCollection<MeetingItems> pagedData = null;

                await Task.Run(() =>
               pagedData = BusinessManager.GetViewMeetings(date, false));

                if (pagedData != null)
                {
                    Items.Clear();
                    for (int i = 0; i < pagedData.Count; i++)
                    {

                        Items.Add(pagedData[i]);
                    }
                }
                else
                {
                    Items.Clear();
                }

                DataPresent = Items.Count > 0;
                DataNotPresent = !DataPresent;
                IsBusy = false;
            }
            catch (ApiCallFailedException ex)
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                return;
            }
            finally
            {
                IsBusy = false;
            }

        }
        private async Task MakeCall(object parameter)
        {
            await Task.Run(() => Device.OpenUri(new Uri("tel:" + parameter)));
        }

    }

}
