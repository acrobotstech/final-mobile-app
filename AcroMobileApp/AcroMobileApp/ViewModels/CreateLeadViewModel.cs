﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using acrobots.mobile;
using AcroMobileApp.BusinessLayer;
using AcroMobileApp.BusinessLayer.Model;
using AcroMobileApp.Views;
using Xamarin.Forms;

namespace AcroMobileApp.ViewModels
{
    public class CreateLeadViewModel : BaseViewModel
    {
        INavigation navigation;

        private string selectedProductType;
        public string SelectedProductType
        {
            get { return selectedProductType; }
            set { selectedProductType = value; OnPropertyChanged(); }
        }

        private string selectedProduct;
        public string SelectedProduct
        {
            get { return selectedProduct; }
            set { selectedProduct = value; OnPropertyChanged(); }
        }

        private string selectedProductText;
        public string SelectedProductText
        {
            get { return selectedProductText; }
            set { selectedProductText = value; OnPropertyChanged(); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged(); }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged(); }
        }

        private string mobileNumber;
        public string MobileNumber
        {
            get { return mobileNumber; }
            set { mobileNumber = value; OnPropertyChanged(); }
        }

        private string loanAmount;
        public string LoanAmount
        {
            get { return loanAmount; }
            set { loanAmount = value; OnPropertyChanged(); }
        }

        private string leadIDText;
        public string LeadIDText
        {
            get { return leadIDText; }
            set { leadIDText = value; OnPropertyChanged(); }
        }

        public ICommand ProductTypeChangedCommand { get; }

        public ICommand CityChangedCommand { get; }

        public ICommand CreateLeadCommand { get; }

        public ICommand MoveToStepTwoCommand { get; }

        public ICommand BackButtonCommand { get; }

        public ICommand DoneCommand { get; }

        public ICommand ApplicationFillCommand { get; set; }

        private Dictionary<string, string> productTypeLookup { get; set; } = new Dictionary<string, string>();

        private Dictionary<string, string> productLookup { get; set; } = new Dictionary<string, string>();

        private ObservableCollection<string> productTypes = new ObservableCollection<string>();
        public ObservableCollection<string> ProductTypes
        {
            get { return productTypes; }
            set { productTypes = value; OnPropertyChanged(); }
        }

        private ObservableCollection<string> products = new ObservableCollection<string>();
        public ObservableCollection<string> Products
        {
            get { return products; }
            set { products = value; OnPropertyChanged(); }
        }

        private Dictionary<int, string> citiesLookup { get; set; } = new Dictionary<int, string>();

        private Dictionary<int, string> branchesLookup { get; set; } = new Dictionary<int, string>();

        private ObservableCollection<string> cities = new ObservableCollection<string>();
        public ObservableCollection<string> Cities
        {
            get { return cities; }
            set { cities = value; OnPropertyChanged(); }
        }

        private ObservableCollection<string> branches = new ObservableCollection<string>();
        public ObservableCollection<string> Branches
        {
            get { return branches; }
            set { branches = value; OnPropertyChanged(); }
        }

        private string selectedCity;
        public string SelectedCity
        {
            get { return selectedCity; }
            set { selectedCity = value; OnPropertyChanged(); }
        }

        private string selectedBranch;
        public string SelectedBranch
        {
            get { return selectedBranch; }
            set { selectedBranch = value; OnPropertyChanged(); }
        }

        private string selectedCityText;
        public string SelectedCityText
        {
            get { return selectedCityText; }
            set { selectedCityText = value; OnPropertyChanged(); }
        }

        private string selectedBranchText;
        public string SelectedBranchText
        {
            get { return selectedBranchText; }
            set { selectedBranchText = value; OnPropertyChanged(); }
        }

        long enteredMobileNo;
        double enteredLoanAmount;

        public CreateLeadViewModel(INavigation navigationRef)
        {
            navigation = navigationRef;

            CreateLeadCommand = new Command(async () => await CreateLead());

            BackButtonCommand = new Command(async () => await navigation.PopAsync());

            ProductTypeChangedCommand = new Command(async () => await GetProductsByProductType());

            CityChangedCommand = new Command(async () => await GetBranchesForCity());

            MoveToStepTwoCommand = new Command(() => MoveToStepTwo());

            DoneCommand = new Command(() => navigation.PopToRootAsync());
        }

        public async Task GetProductTypes()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                try
                {
                    List<ProductTypeDetailItem> productTypeList = BusinessManager.GetProductTypes();

                    foreach (ProductTypeDetailItem productType in productTypeList)
                    {
                        Device.BeginInvokeOnMainThread(() => ProductTypes.Add(productType.name));
                        productTypeLookup.Add(productType.id, productType.name);
                    }

                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }

                IsBusy = false;
            });
        }

        public async Task GetProductsByProductType()
        {
            Products.Clear();
            SelectedProduct = string.Empty;
            SelectedProductText = string.Empty;
            productLookup.Clear();
            await Task.Run(() =>
            {
                IsBusy = true;
                try { 
                string productId = productTypeLookup.FirstOrDefault(x => x.Value == SelectedProductType).Key;
                List<ProductDetailItem> productsByProductType = BusinessManager.GetProducts(productId);
                foreach (ProductDetailItem product in productsByProductType)
                {
                    Device.BeginInvokeOnMainThread(() => Products.Add(product.name));
                    productLookup.Add(product.id, product.name);
                }
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }
                IsBusy = false;
            });
        }

        public async Task GetCities()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                try { 
                List<CityDetailItem> citiesList = BusinessManager.GetCities();
                 citiesLookup.Clear();
                foreach (CityDetailItem city in citiesList)
                {
                    Device.BeginInvokeOnMainThread(() => Cities.Add(city.Name));
                    citiesLookup.Add(city.CityId, city.Name);
                }
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }
                IsBusy = false;
            });
        }

        async Task GetBranchesForCity()
        {
            Branches.Clear();
            SelectedBranch = string.Empty;
            SelectedBranchText = string.Empty;
            branchesLookup.Clear();
            await Task.Run(() =>
            {
                IsBusy = true;
                try { 
                int cityId = citiesLookup.FirstOrDefault(x => x.Value == SelectedCity).Key;
                List<BranchDetailItem> branchesByCity = BusinessManager.GetBranches(cityId);
                foreach (BranchDetailItem branch in branchesByCity)
                {
                    Device.BeginInvokeOnMainThread(() => Branches.Add(branch.Name));
                    branchesLookup.Add(branch.BranchId, branch.Name);
                }
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }


                IsBusy = false;
            });
        }

        private void MoveToStepTwo()
        {
            if (ValidateFirstStep())
                navigation.PushAsync(new CreateLeadPage2(this, navigation));
        }

        async Task CreateLead()
        {
            if (ValidateSecondStep())
            {
                await Task.Run(() =>
                {
                    IsBusy = true;
                    try { 
                    string productId = productLookup.FirstOrDefault(x => x.Value == SelectedProduct).Key;
                    int branchId = branchesLookup.FirstOrDefault(x => x.Value == SelectedBranch).Key;

                    ApiResponse response = BusinessManager.CreateLead(new CreateLeadDetailItem(productId, Name, Email, MobileNumber, branchId.ToString(), LoanAmount));
                        if (response.responseCode == 0 && response.Id > 0)
                        {
                            ApplicationFillCommand = new Command(() => navigation.PushAsync(new ApplicationFillPage(navigation, response.Id)));
                            LeadIDText = "Lead ID: " + response.Id;
                            Device.BeginInvokeOnMainThread(() => navigation.PushAsync(new CreateLeadPage3(this, navigation)));
                        }
                        else
                        {
                            Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(response.messages?.messageDescription));
                        }
                    }
                    catch (ApiCallFailedException ex)
                    {
                        Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                        return;
                    }
                    finally
                    {
                        IsBusy = false;
                    }

                    IsBusy = false;
                });
            }
        }

        private bool ValidateFirstStep()
        {
            if (string.IsNullOrEmpty(SelectedProductType))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Select a product type");
                return false;
            }

            if (string.IsNullOrEmpty(SelectedProduct))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Select a product");
                return false;
            }

            return true;
        }

        private bool ValidateSecondStep()
        {
            if (string.IsNullOrEmpty(Name))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter name");
                return false;
            }

            if (string.IsNullOrEmpty(Email))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter email");
                return false;
            }

            if (!Regex.IsMatch(Email, "[\\w-]+@([\\w-]+\\.)+[\\w-]+"))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter valid email address");
                return false;
            }

            if (string.IsNullOrEmpty(MobileNumber))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter mobile number");
                return false;
            }

            if (MobileNumber.Length != 10)
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Invalid mobile number");
                return false;
            }

            if (!long.TryParse(MobileNumber, out enteredMobileNo))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Invalid mobile number");
                return false;
            }

            if (string.IsNullOrEmpty(SelectedCity))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Select city");
                return false;
            }

            if (string.IsNullOrEmpty(SelectedBranch))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Select branch");
                return false;
            }

            if (String.IsNullOrEmpty(LoanAmount))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter loan amount");
                return false;
            }

            if (!double.TryParse(LoanAmount, out enteredLoanAmount))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Invalid loan amount");
                return false;
            }

            if (enteredLoanAmount <= 0.0)
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Loan amount should be more than 0");
                return false;
            }

            return true;
        }
    }
}
