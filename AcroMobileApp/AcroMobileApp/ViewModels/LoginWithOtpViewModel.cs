﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using System.Threading.Tasks;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using AcroMobileApp.BusinessLayer.Model;
using SQLiteAsync.Services;
using acrobots.mobile.BusinessLayer;
using acrobots.mobile;

namespace AcroMobileApp.ViewModels
{
    public class LoginWithOtpViewModel : BaseViewModel
    {
        INavigation navigation;

        private string mobileNumber;
        public string MobileNumber
        {
            get { return mobileNumber; }
            set { mobileNumber = value; OnPropertyChanged(); }
        }

        private string digit1;
        public string Digit1
        {
            get { return digit1; }
            set { digit1 = value; OnPropertyChanged(); }
        }

        private string digit2;
        public string Digit2
        {
            get { return digit2; }
            set { digit2 = value; OnPropertyChanged(); }
        }

        private string digit3;
        public string Digit3
        {
            get { return digit3; }
            set { digit3 = value; OnPropertyChanged(); }
        }

        private string digit4;
        public string Digit4
        {
            get { return digit4; }
            set { digit4 = value; OnPropertyChanged(); }
        }

        public ICommand BackButtonCommand
        {
            get;
        }

        public ICommand VerifyOtpCommand
        {
            get;
        }

        public ICommand ResendOtpCommand
        {
            get;
        }

        public LoginWithOtpViewModel(INavigation navigationRef, string mobileNo)
        {
            navigation = navigationRef;
            MobileNumber = mobileNo;
            BackButtonCommand = new Command(async () => await navigation.PopAsync());
            VerifyOtpCommand = new Command(async () => await VerifyOtp());
            ResendOtpCommand = new Command(async () => await ResendOtp());
        }

        async Task VerifyOtp()
        {
            if (IsInputValid())
            {
                IsBusy = true;
                try
                {
                    VerifyCredentialsResponse response = await Task.Run(() => BusinessManager.VerifyOtpForLogin(MobileNumber, Digit1 + Digit2 + Digit3 + Digit4));
                    IsBusy = false;
                    if (response.responseCode == 0)
                    {
                        Auth auth = new Auth();
                        auth.accessToken = response.accessToken;
                        auth.expiresAt = DateTime.Now;
                        auth.tokenType = response.token_type;
                        DatabaseService.Instance.AddItem<Auth>(auth);
                        App.GotoHomePage();
                        return;
                    }

                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(response.messages.messageDescription));
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        async Task ResendOtp()
        {
            await Task.Run(() =>
            {
                try { 
                IsBusy = true;
                ApiResponse response = BusinessManager.ResendOtpForLogin(MobileNumber);
                IsBusy = false;
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(response.messages.messageDescription));
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }
            });
        }

        private bool IsInputValid()
        {
            int otp;
            if (!int.TryParse(Digit1, out otp))
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage("Only digits are allowed"));
                return false;
            }

            if (!int.TryParse(Digit2, out otp))
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage("Only digits are allowed"));
                return false;
            }

            if (!int.TryParse(Digit3, out otp))
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage("Only digits are allowed"));
                return false;
            }

            if (!int.TryParse(Digit4, out otp))
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage("Only digits are allowed"));
                return false;
            }
            return true;
        }
    }
}
