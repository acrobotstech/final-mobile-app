﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AcroMobileApp.Models;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using System.Windows.Input;
using AcroMobileApp.BusinessLayer.Model;
using System.Collections.Generic;
using acrobots.mobile;

namespace AcroMobileApp.ViewModels
{
    class ChangeLeadStatusViewModel : BaseViewModel
    {

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; OnPropertyChanged(); }
        }


        private string comments;
        public string Comments
        {
            get { return comments; }
            set { comments = value; OnPropertyChanged(); }
        }

        private string selectedReasonText;
        public string SelectedReasonText
        {
            get { return selectedReasonText; }
            set { selectedReasonText = value; OnPropertyChanged(); }
        }


        public ICommand BackButtonCommand { get; }

        public ICommand UpdateStatusCommand { get; }

        List<String> reasonList;

        public List<String> ReasonList
        {
            get { return reasonList; }
            set { reasonList = value; OnPropertyChanged(); }
        }

        private ObservableCollection<string> reasons = new ObservableCollection<string>();
        public ObservableCollection<string> Reasons
        {
            get { return reasons; }
            set { reasons = value; OnPropertyChanged(); }
        }

        int LeadId;
        INavigation navigation;
        public ChangeLeadStatusViewModel(INavigation navigationRef, int leadId)
        {
            this.navigation = navigationRef;
            this.LeadId = leadId;
            BackButtonCommand = new Command(async () => await navigation.PopAsync());

            UpdateStatusCommand = new Command(async () => await UpdateLeadStatus());
            Device.BeginInvokeOnMainThread(() =>
            {

                IsBusy = true;

                try
                {
                    Reasons = BusinessManager.GetAllChangeStatusReasons();                   
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }

                IsBusy = false;
            });

        }



        async Task UpdateLeadStatus()
        {
            if (IsInputValid())
            {
                await Task.Run(() =>
                {
                    IsBusy = true;
                    try
                    {

                        ApiResponse response = BusinessManager.ChangeLeadStatus(LeadId.ToString(), selectedReasonText, Comments, Status);
                        IsBusy = false;
                        if (response.responseCode == 0)
                        {
                            Device.BeginInvokeOnMainThread(() => navigation.PopAsync());
                            Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(response.messages.messageDescription));
                            return;
                        }

                    }
                    catch (ApiCallFailedException ex)
                    {
                        Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                        return;
                    }
                    finally
                    {
                        IsBusy = false;
                    }


                });
            }
        }


        private bool IsInputValid()
        {

            if (string.IsNullOrEmpty(selectedReasonText))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Select reason");
                return false;
            }

            if (string.IsNullOrEmpty(Comments))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter comment");
                return false;
            }




            return true;
        }


    }

}
