﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AcroMobileApp.Models;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using System.Windows.Input;
using acrobots.mobile;

namespace AcroMobileApp.ViewModels
{
    public class ViewLeadsViewModel : BaseViewModel
    {

        public ObservableCollection<LeadDetailsItem> Items { get; set; }
        public Command LoadItemsCommand { get; set; }
        public Command MakeCallCommand { get; set; }

        private long mobileNo = 9999;
        public long MobileNo
        {
            get { return mobileNo; }
            set { mobileNo = value; OnPropertyChanged(); }
        }

        bool dataPresent = false;
        public bool DataPresent
        {
            get { return dataPresent; }
            set { dataPresent = value; OnPropertyChanged(); }
        }

        bool dataNotPresent = false;
        public bool DataNotPresent
        {
            get { return dataNotPresent; }
            set { dataNotPresent = value; OnPropertyChanged(); }
        }


        bool noFurtherData = false;
        public bool NoFurtherData
        {
            get { return noFurtherData; }
            set { noFurtherData = value; OnPropertyChanged(); }
        }

        INavigation navigation;
        private int DEFAULT_PAGE_SIZE = 10;
        private int CURRENT_PAGE_NO = 1;
        private string Status;
        public ViewLeadsViewModel(INavigation navigationRef, String status)
        {
            navigation = navigationRef;
            Status = status;
            Title = "Browse";
            Items = new ObservableCollection<LeadDetailsItem>();
            LoadItemsCommand = new Command(async () => await LoadItemsWithPaging());
            MakeCallCommand = new Command(async (parameter) => await MakeCall(parameter));

            Device.BeginInvokeOnMainThread(() =>
            {
                if (IsBusy)
                    return;

                IsBusy = true;

                try
                {

                    ObservableCollection<LeadDetailsItem> pagedData = BusinessManager.GetViewLeads(CURRENT_PAGE_NO, DEFAULT_PAGE_SIZE, Status);
                    CURRENT_PAGE_NO = CURRENT_PAGE_NO + 1;
                    for (int i = 0; i < pagedData.Count; i++)
                    {
                        String formattedAmount = getFormattedAmount(pagedData[i].Amount);
                        pagedData[i].Amount = formattedAmount;
                        Items.Add(pagedData[i]);
                    }

                    DataPresent = Items.Count > 0;
                    DataNotPresent = !DataPresent;
                   IsBusy = false;

                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }

            });
        }

         public int getIndexOf(LeadDetailsItem item)
        {
            return Items.IndexOf(item);
        }

        
       
        public static String getFormattedAmount(String amount)
        {

            if(amount == null || amount == "")
            {
                amount = "00.00";
            }
            double amountValue = Convert.ToDouble(amount);
            amount = "₹" + amountValue.ToString("###,###,###.##");

            return amount;

        }

        public async Task LoadItemsWithPaging()
        {

            if (IsBusy)
                return;

            IsBusy = true;


            ObservableCollection<LeadDetailsItem> pagedData = null;
            await Task.Run(() => pagedData = BusinessManager.GetViewLeads(CURRENT_PAGE_NO, DEFAULT_PAGE_SIZE, Status));
            try
            { 


               
            CURRENT_PAGE_NO = CURRENT_PAGE_NO + 1;
                

                for (int i = 1; i < pagedData.Count; i++)
                {                   
                    pagedData[i].Amount = getFormattedAmount(pagedData[i].Amount);
                    Items.Add(pagedData[i]);
                }

                if(pagedData != null && pagedData.Count < 1)
                {
                    noFurtherData = true;
                }
            }
            catch (ApiCallFailedException ex)
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                return;
            }
            finally
            {
                IsBusy = false;
            }


           
        }

        private async Task MakeCall(object parameter)
        {
           await Task.Run(() => Device.OpenUri(new Uri("tel:"+ parameter)));
        }
    }
}