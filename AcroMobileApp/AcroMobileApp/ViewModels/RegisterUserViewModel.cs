﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using acrobots.mobile;
using AcroMobileApp.BusinessLayer;
using AcroMobileApp.BusinessLayer.Model;
using AcroMobileApp.Views;
using Xamarin.Forms;

namespace AcroMobileApp.ViewModels
{
    public class RegisterUserViewModel : BaseViewModel
    {
        INavigation navigation;

        private string registerAs;
        public string RegisterAs
        {
            get { return registerAs; }
            set { registerAs = value; OnPropertyChanged(); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged(); }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged(); }
        }

        private string mobileNumber;
        public string MobileNumber
        {
            get { return mobileNumber; }
            set { mobileNumber = value; OnPropertyChanged(); }
        }

        public ICommand SaveUserDetailsCommand { get; }

        public ICommand CityChangedCommand { get; }

        public ICommand BackButtonCommand { get; }

        public ICommand LoginNowCommand { get; }

        private Dictionary<int, string> citiesLookup { get; set; } = new Dictionary<int, string>();

        private Dictionary<int, string> branchesLookup { get; set; } = new Dictionary<int, string>();

        private ObservableCollection<string> cities = new ObservableCollection<string>();
        public ObservableCollection<string> Cities
        {
            get { return cities; }
            set { cities = value; OnPropertyChanged(); }
        }

        private ObservableCollection<string> branches = new ObservableCollection<string>();
        public ObservableCollection<string> Branches
        {
            get { return branches; }
            set { branches = value; OnPropertyChanged(); }
        }

        private string selectedCity;
        public string SelectedCity
        {
            get { return selectedCity; }
            set { selectedCity = value; OnPropertyChanged(); }
        }

        private string selectedBranch;
        public string SelectedBranch
        {
            get { return selectedBranch; }
            set { selectedBranch = value; OnPropertyChanged(); }
        }

        private string selectedBranchText;
        public string SelectedBranchText
        {
            get { return selectedBranchText; }
            set { selectedBranchText = value; OnPropertyChanged(); }
        }

        long enteredMobileNo;

        public RegisterUserViewModel(INavigation navigationRef)
        {
            navigation = navigationRef;
            SaveUserDetailsCommand = new Command(async () => await SaveUserDetails());
            CityChangedCommand = new Command(async () => await GetBranchesForCity());
            BackButtonCommand = new Command(async () => await navigation.PopAsync());
            LoginNowCommand = new Command(async () => await navigation.PushAsync(new LoginPage()));

            Task.Run(() =>
            {
                IsBusy = true;
                try
                {
                    List<CityDetailItem> citiesList = BusinessManager.GetCities();

                    foreach (CityDetailItem city in citiesList)
                    {
                        Device.BeginInvokeOnMainThread(() => Cities.Add(city.Name));
                        citiesLookup.Add(city.CityId, city.Name);
                    }
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }
                IsBusy = false;
            });
        }

        async Task SaveUserDetails()
        {
            if (IsInputValid())
            {
                await Task.Run(() =>
                {
                    IsBusy = true;
                    try
                    {
                        int branchId = branchesLookup.FirstOrDefault(x => x.Value == SelectedBranch).Key;
                        ApiResponse response = BusinessManager.RegisterUser(RegisterAs, Name, Email, MobileNumber, branchId);
                        IsBusy = false;
                        if (response.responseCode == 0)
                        {
                            Device.BeginInvokeOnMainThread(() => navigation.PushAsync(new LoginPage()));
                            Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(response.messages.messageDescription));
                            return;
                        }

                    }
                    catch (ApiCallFailedException ex)
                    {
                        Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                        return;
                    }
                });
            }
        }

        async Task GetBranchesForCity()
        {
            Branches.Clear();
            SelectedBranch = string.Empty;
            SelectedBranchText = string.Empty;
            branchesLookup.Clear();
            await Task.Run(() =>
            {
                IsBusy = true;
                try
                {
                    Device.BeginInvokeOnMainThread(() => Branches.Clear());
                    int cityId = citiesLookup.FirstOrDefault(x => x.Value == SelectedCity).Key;
                    List<BranchDetailItem> branchesByCity = BusinessManager.GetBranches(cityId);
                    foreach (BranchDetailItem branch in branchesByCity)
                    {
                        Device.BeginInvokeOnMainThread(() => Branches.Add(branch.Name));
                        branchesLookup.Add(branch.BranchId, branch.Name);
                    }
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                IsBusy = false;
            });
        }

        private bool IsInputValid()
        {
            if (string.IsNullOrEmpty(Name))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter name");
                return false;
            }

            if (string.IsNullOrEmpty(Email))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter email");
                return false;
            }

            if (!Regex.IsMatch(Email, "[\\w-]+@([\\w-]+\\.)+[\\w-]+"))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter valid email address");
                return false;
            }

            if (string.IsNullOrEmpty(MobileNumber))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Enter mobile number");
                return false;
            }

            if (MobileNumber.Length < 10)
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Invalid mobile number");
                return false;
            }

            if (!long.TryParse(MobileNumber, out enteredMobileNo))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Invalid mobile number");
                return false;
            }

            if (string.IsNullOrEmpty(SelectedCity))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Select city");
                return false;
            }

            if (string.IsNullOrEmpty(SelectedBranch))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Select branch");
                return false;
            }

            return true;
        }
    }
}
