﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AcroMobileApp.Models;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using System.Windows.Input;
using AcroMobileApp.BusinessLayer.Model;
using acrobots.mobile;

namespace AcroMobileApp.ViewModels
{
    class LeadInfoViewModel : BaseViewModel
    {
        string heightList;
        int heightRowsList = 40;
        public ObservableCollection<ArrayOfNote> AllNotes { get; set; }

        public string HeightList
        {
            get
            {
                if (AllNotes == null)
                    return (heightRowsList).ToString();
                heightList = (AllNotes.Count * heightRowsList).ToString();
                return heightList;
            }

            set
            {
                heightList = value;
                OnPropertyChanged();
            }
        }

        public Command MakeCallCommand { get; set; }
        public Command SaveNotesCommand { get; set; }



        private Boolean isServicerPresent;
        public Boolean IsServicerPresent
        {
            get { return isServicerPresent; }
            set { isServicerPresent = value; OnPropertyChanged(); }
        }

        private String name;
        public String Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged(); }
        }

        private String note;
        public String Note
        {
            get { return note; }
            set { note = value; OnPropertyChanged(); }
        }

        private String earning;
        public String Earning
        {
            get { return earning; }
            set { earning = value; OnPropertyChanged(); }
        }

        private String loanAmount;
        public String LoanAmount
        {
            get { return loanAmount; }
            set { loanAmount = value; OnPropertyChanged(); }
        }


        public LeadDetailsResponse Response { get; set; }

        public ICommand BackButtonCommand { get; }

        INavigation navigation;
        int LeadId;
        public LeadInfoViewModel(INavigation navigationRef, int LeadId)
        {
            this.navigation = navigationRef;
            this.LeadId = LeadId;
            AllNotes = new ObservableCollection<ArrayOfNote>();
            BackButtonCommand = new Command(async () => await navigation.PopAsync());
            MakeCallCommand = new Command(async (parameter) => await MakeCall(parameter));
            SaveNotesCommand = new Command(async (parameter) => await saveNote());
            getLeadDetails();


        }

        public async Task saveNote()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                Device.BeginInvokeOnMainThread(() => callSaveNoteApi());
                IsBusy = false;
            });
        }

        private void callSaveNoteApi()
        {
            if (IsInputValid())
            {
                try
                {
                    ApiResponse resp = BusinessManager.SaveNote(LeadId, note);
                    if (resp.responseCode == 0)
                    {

                        ArrayOfNote newNote = new ArrayOfNote();
                        newNote.CreatedDateTime = new DateTime();
                        newNote.Notes = Note;
                        AllNotes.Insert(0, newNote);
                        Note = "";
                        String height = HeightList;
                        Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(resp.messages.messageDescription));
                    }
                }
                catch (ApiCallFailedException ex)
                {
                    Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                    return;
                }
                finally
                {
                    IsBusy = false;
                }

            }
        }

        public async Task GetLeadDetail()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                Device.BeginInvokeOnMainThread(() => getLeadDetails());
                IsBusy = false;
            });
        }

        private void getLeadDetails()
        {
            try
            {

                Response = BusinessManager.GetCompleteLeadDetails(LeadId);
                Earning = getFormattedAmount(Response.CommissionAmount);
                LoanAmount = getFormattedAmount(Response.LoanAmount);
                if (!String.IsNullOrEmpty(Response.BankRMName))
                {
                    isServicerPresent = true;
                }
                if (Response.ArrayOfNotes != null)
                {
                    for (int i = 0; i < Response.ArrayOfNotes.Count; i++)
                    {
                        AllNotes.Add(Response.ArrayOfNotes[i]);
                    }
                }
            }
            catch (ApiCallFailedException ex)
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage(ex.Message));
                return;
            }
            finally
            {
                IsBusy = false;
            }


        }

        public static String getFormattedAmount(Double amount)
        {


            double amountValue = amount;
            return "₹" + amountValue.ToString("###,###,###.##");



        }

        private async Task MakeCall(object parameter)
        {
            await Task.Run(() => Device.OpenUri(new Uri("tel:" + parameter)));
        }


        private bool IsInputValid()
        {
            if (String.IsNullOrEmpty(Note) || String.IsNullOrWhiteSpace(Note))
            {
                DependencyService.Get<IDisplayMessage>().DisplayMessage("Please enter note.");
                return false;
            }


            return true;
        }

    }

}
