﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace AcroMobileApp.ViewModels
{
    public class ViewDocumentViewModel : BaseViewModel
    {
        INavigation navigation;
        private string source;
        public string Source
        {
            get { return source; }
            set { source = value; OnPropertyChanged(); }
        }
        public ICommand BackButtonCommand { get; }

        public ViewDocumentViewModel(INavigation navigationRef, string documentSource)
        {
            this.navigation = navigationRef;
            BackButtonCommand = new Command(async () => await navigation.PopAsync());

            Source = documentSource;
        }
    }
}
