﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AcroMobileApp.Models;
using AcroMobileApp.Views;
using AcroMobileApp.BusinessLayer;
using System.Windows.Input;

namespace AcroMobileApp.ViewModels
{
    class ViewLeadsPageModel : BaseViewModel
    {
        private bool isLeadInfoTabSelected;
        public bool IsLeadInfoTabSelected
        {
            get { return isLeadInfoTabSelected; }
            set { isLeadInfoTabSelected = value; OnPropertyChanged(); }
        }

        private bool isPersonalInfoTabSelected;
        public bool IsPersonalInfoTabSelected
        {
            get { return isPersonalInfoTabSelected; }
            set { isPersonalInfoTabSelected = value; OnPropertyChanged(); }
        }

        private bool isPropertyInfoTabSelected;
        public bool IsPropertyInfoTabSelected
        {
            get { return isPropertyInfoTabSelected; }
            set { isPropertyInfoTabSelected = value; OnPropertyChanged(); }
        }

        private bool isLoginInfoTabSelected;
        public bool IsLoginInfoTabSelected
        {
            get { return isLoginInfoTabSelected; }
            set { isLoginInfoTabSelected = value; OnPropertyChanged(); }
        }

        public ICommand TabIndexChangedCommand { get; set; }

        
        public ICommand BackButtonCommand { get; }
        Boolean IsShowAll;
        INavigation navigation;
        public ViewLeadsPageModel(INavigation navigationRef, Boolean isShowAll)
        {
            IsShowAll = isShowAll;
            IsLeadInfoTabSelected = true;
            TabIndexChangedCommand = new Command((parameter) => TabIndexChanged(parameter));
            navigation = navigationRef;
            BackButtonCommand = new Command(async () => await navigation.PushAsync(new MainPage()));

        }


        private void TabIndexChanged(object parameter)
        {
            string tabInfo = parameter as string;

            IsLeadInfoTabSelected = false;
            IsPersonalInfoTabSelected = false;
            IsPropertyInfoTabSelected = false;
            IsLoginInfoTabSelected = false;

            switch (parameter)
            {
                case "IN PROGRESS":
                    IsLeadInfoTabSelected = true;
                    break;
                case "LOGIN":
                    IsPersonalInfoTabSelected = true;
                    break;
                case "DISBURSED":
                    IsPropertyInfoTabSelected = true;
                    break;
                case "REJECTED":
                    IsLoginInfoTabSelected = true;
                    break;
                default:
                    IsLeadInfoTabSelected = true;
                    break;
            }
        }

    }

}
