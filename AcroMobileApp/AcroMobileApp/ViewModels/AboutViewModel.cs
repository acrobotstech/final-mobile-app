﻿using AcroMobileApp.Views;
using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace AcroMobileApp.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public ICommand BackButtonCommand { get; }
        public AboutViewModel(INavigation navigationRef)
        {
            Title = "About";
            BackButtonCommand = new Command(async () => await navigationRef.PushAsync(new MainPage()));
            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("https://xamarin.com/platform")));
        }

        public ICommand OpenWebCommand { get; }
    }
}