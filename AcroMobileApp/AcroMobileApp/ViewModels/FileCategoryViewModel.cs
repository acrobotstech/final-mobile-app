﻿using System;
namespace AcroMobileApp.ViewModels
{
    public class FileCategoryViewModel : BaseViewModel
    {
        private string header;
        public string Header
        {
            get { return header; }
            set { header = value; OnPropertyChanged(); }
        }

        public FileCategoryViewModel(string categoryName)
        {
            Header = categoryName;
        }
    }
}
