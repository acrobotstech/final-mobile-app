﻿using System;

public class StateInformation
{
    public enum RefreshType
    {
        Bills,
        Payments,
        Service,
        UserDetails,
        ServiceSingle,
        Config,
        ServicesForUser
    };

    private static StateInformation _instance;

   

    private StateInformation()
    {
    }

    public static StateInformation Instance => _instance ?? (_instance = new StateInformation());

    public int CurrentAdvert { get; set; }

    public void ClearStateInformation()
    {
        _instance = null;
    }
}