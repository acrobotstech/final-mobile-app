﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace acrobots.mobile
{
    public class ApiCallFailedException : Exception
    {
        public HttpResponseMessage HttpMessage { get; set; }
        public int ResponseCode;
        
        public ApiCallFailedException()
        {
        }

        public ApiCallFailedException(HttpResponseMessage message)
        {
            HttpMessage = message;
        }

        public ApiCallFailedException(string message) : base(message)
        {
          
        }

        public ApiCallFailedException(string message, int responseCode) : base(message)
        {
            this.ResponseCode = responseCode;
        }

        public ApiCallFailedException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}