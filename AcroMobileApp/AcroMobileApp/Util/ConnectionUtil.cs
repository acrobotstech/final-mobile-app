﻿
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace AcroMobileApp.Util
{
    public static class ConnectionUtil
    {

        public static bool IsInternetAvailable()
        {
            try
            {
                Ping myPing = new Ping();
                String host = "www.google.com";
                byte[] buffer = new byte[32];
                int timeout = 1200;
                PingOptions pingOptions = new PingOptions();                
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return reply.Status == IPStatus.Success;

            }
            catch (Exception)
            {
                return false;
            }
        }

      

    }
}
