﻿namespace acrobots.mobile
{
    public class Message
    {
        private Message() { }

        public enum MessageType
        {
            Error,
            Success,
            Notify,
        };
    }
}
