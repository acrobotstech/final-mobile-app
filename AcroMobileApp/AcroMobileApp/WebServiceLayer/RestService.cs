﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using acrobots.mobile.BusinessLayer;
using Newtonsoft.Json;

using Xamarin.Forms.Internals;
using System.Threading.Tasks;
using Xamarin.Forms;
using acrobots.ResX;
using System.Collections.Generic;
using AcroMobileApp;
using AcroMobileApp.BusinessLayer;

namespace acrobots.mobile.Web
{
    [Preserve(AllMembers = true)]
    public class RestService
    {
        private readonly string _baseUrl = "http://api.apnarupee.in/api";

        private readonly HttpClient _client;

        public enum RestMethod {Get, Post};

        public RestService()
        {
            _client = new HttpClient();
        }

        public T MakeGetRestCall<T>(string url)
        {
            return RestCall<T>(url, RestMethod.Get, BusinessManager.GetToken()?.accessToken, true, null);
        }

        public T MakePostRestCall<T>(string url, object postObject)
        {
            return RestCall<T>(url, RestMethod.Post, BusinessManager.GetToken()?.accessToken, true, postObject);
        }

        // authString will be either user:password:access_number or the token that we have
        public T RestCall<T>(string url, RestMethod method, string authString, bool retry, object postObject)
        {

            // Check if the user currently has internet
            // APP-492 TEMPORARILY DISABLED THIS CODE
            // On some phones (specifically Andrew MacEwan's Galaxy S9) this IsConnected check returns
            // false even when the phone is connected. Further investigation required but disabling
            // the check for now.
            // 
            //if (!Plugin.Connectivity.CrossConnectivity.Current.IsConnected)
            //{
            //    Device.BeginInvokeOnMainThread(() =>
            //    {
            //        DependencyService.Get<IDisplayMessage>().DisplayMessage(AppResources.NoNetworkConnection, Message.MessageType.Error);
            //    });
            //    System.Diagnostics.Debug.WriteLine(method + "call to " + url + "has failed due to no network connection");
            //    throw new ApiCallFailedException();
            //}

            retry = false;
            var uri = new Uri(string.Format(_baseUrl + url, string.Empty));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authString);

            // Sensible timeout is 10 seconds
            _client.Timeout = new TimeSpan(0, 0, 0, 25, 0);

            HttpResponseMessage response = null;

            try
            {
                response = new HttpResponseMessage();

                System.Diagnostics.Debug.WriteLine(uri);
                switch (method)
                {
                    case (RestMethod.Get):
                        response = _client.GetAsync(uri, HttpCompletionOption.ResponseHeadersRead).Result;
                        break;
                    case (RestMethod.Post):
                        string json = JsonConvert.SerializeObject(postObject);
                        var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                        System.Diagnostics.Debug.WriteLine(httpContent);
                        response = _client.PostAsync(uri, httpContent).Result;
                        break;
                }
                System.Diagnostics.Debug.WriteLine(response);
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    System.Diagnostics.Debug.WriteLine(typeof(T).ToString() + " call:" + x.Message + "\n" + x.StackTrace);
                    // Return null to indicate total failure (no response) vs received failure response.

                    if (x is TaskCanceledException) //API timed out
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DependencyService.Get<IDisplayMessage>().DisplayMessage("Connection timeout, please check internet connection.");
                        });
                        throw new ApiCallFailedException("Connection timeout, please check internet connection.", 2);
                    }

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DependencyService.Get<IDisplayMessage>().DisplayMessage("Internal server error.");
                    });

                    throw new ApiCallFailedException("Internal server error.");
                });
            }

            if (response.StatusCode.Equals(HttpStatusCode.Unauthorized) && retry)
            {
                // Try and get a new token and try again
                string updatedAuthString = "";
                if (updatedAuthString == null)
                {
                    // If we couldn't re-authenticate then API call is failure. Might need
                    // some better logic and user feedback for this.
                    throw new ApiCallFailedException();
                }
                return RestCall<T>(url, method, "", false, postObject);
            }

            if (response.IsSuccessStatusCode)
            {
                return GetContent<T>(response);
            }

            throw new ApiCallFailedException("Internal server error, please retry after sometime.");
        }

        public T GetContent<T>(HttpResponseMessage response)
        {
            var content = response.Content.ReadAsStringAsync().Result;
            System.Diagnostics.Debug.WriteLine(content);
            return JsonConvert.DeserializeObject<T>(content);
        }

        private T ReadJsonFromFile<T>(string url)
        {
            var assembly = typeof(RestService).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(url);

            using (var streamReader = new StreamReader(stream))
            {
                string json = streamReader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(json);
            }
        }
    }
}