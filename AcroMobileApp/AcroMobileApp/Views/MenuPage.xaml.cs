﻿using AcroMobileApp.DataLayer;
using AcroMobileApp.Models;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => ((NavigationPage)Application.Current.MainPage).RootPage as MainPage; }
        List<HomeMenuItem> menuItems;
        public MenuPage()
        {
            InitializeComponent();

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.MyLeads, Title="My Leads", Icon="mlist.png" },
                new HomeMenuItem {Id = MenuItemType.Notification, Title="Notification", Icon="mnotification.png" },
                new HomeMenuItem {Id = MenuItemType.Calculators, Title="Calculators",Icon="mcalculator_1.png" },                           
                new HomeMenuItem {Id = MenuItemType.Meetings, Title="Meetings", Icon="interview.png" },
                new HomeMenuItem {Id = MenuItemType.AboutUs, Title="About Us", Icon="mcare_about_planet.png" },
                new HomeMenuItem {Id = MenuItemType.ContactUs, Title="Contact Us", Icon="mphone_call.png" },
                new HomeMenuItem {Id = MenuItemType.Logout, Title="Logout", Icon="mlogout_1.png" }
            };

            ListViewMenu.ItemsSource = menuItems;

            
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;

                if (id==8)
                {
                    Device.BeginInvokeOnMainThread(async () => {
                        var result = await this.DisplayAlert("Alert!", "Are you sure you want to logout?", "Yes", "No");
                        if (result) {

                            DatabaseLayer.TruncateAllTables();
                            StateInformation.Instance.ClearStateInformation();
                            App.GotoLoginPage();
                        }
                    });

                } else {
                    await RootPage.NavigateFromMenu(id);

                }

                
            };
        }
    }
}