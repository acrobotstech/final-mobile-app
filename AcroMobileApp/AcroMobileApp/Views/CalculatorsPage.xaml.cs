﻿using acrobots.mobile;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
    public partial class CalculatorsPage : ContentPage
    {
        CalculatorViewModel viewModel { get; set; }
        public CalculatorsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = new CalculatorViewModel(Navigation);
            BindingContext = viewModel;

        }

        void webviewNavigating(object sender, WebNavigatingEventArgs e)
        {
            indicator.IsVisible = true;
            indicator.IsRunning = true;
        }

        void webviewNavigated(object sender, WebNavigatedEventArgs e)
        {
            indicator.IsVisible = false;
            indicator.IsRunning = false;
        }
    }
}
