﻿using acrobots.mobile;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
    public partial class ContactUsPage : ContentPage
    {
        ContactUsViewModel viewModel { get; set; }
        public ContactUsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = new ContactUsViewModel(Navigation);
            BindingContext = viewModel;
        }
    }
}
