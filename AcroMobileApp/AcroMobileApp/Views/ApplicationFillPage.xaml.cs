﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using AcroMobileApp.ViewModels;

namespace AcroMobileApp.Views
{
    public partial class ApplicationFillPage : ContentPage
    {
        ApplicationFillViewModel viewModel;
        public ApplicationFillPage(INavigation navigation, int leadId)
        {
            InitializeComponent();
            viewModel = new ApplicationFillViewModel(navigation, leadId);
            BindingContext = viewModel;
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.DoOnFirstAppearanceAsync(viewModel.FetchAllData);
        }
    }
}
