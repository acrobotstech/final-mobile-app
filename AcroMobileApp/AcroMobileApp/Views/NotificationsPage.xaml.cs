﻿using acrobots.mobile;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
    public partial class NotificationsPage : ContentPage
    {
        NotificationViewModel viewModel { get; set; }
        public NotificationsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = new NotificationViewModel(Navigation);
            BindingContext = viewModel;
        }
    }
}
