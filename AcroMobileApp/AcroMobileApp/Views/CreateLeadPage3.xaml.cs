﻿using System;
using System.Collections.Generic;
using AcroMobileApp.ViewModels;
using Xamarin.Forms;

namespace AcroMobileApp.Views
{
    public partial class CreateLeadPage3 : ContentPage
    {
        public CreateLeadPage3(CreateLeadViewModel vm, INavigation navigation)
        {
            InitializeComponent();
            BindingContext = vm;
        }
    }
}