﻿using AcroMobileApp.ViewModels;
using Xamarin.Forms;

namespace AcroMobileApp.Views
{
    public partial class CreateLeadPage2 : ContentPage
    {
        CreateLeadViewModel viewModel;
        public CreateLeadPage2(CreateLeadViewModel vm, INavigation navigation)
        {
            InitializeComponent();
            viewModel = vm;
            BindingContext = viewModel;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            await viewModel.DoOnFirstAppearanceAsync(viewModel.GetCities);
        }
    }
}
