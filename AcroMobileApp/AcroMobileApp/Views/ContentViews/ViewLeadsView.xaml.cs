﻿using AcroMobileApp.Models;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views.ContentViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ViewLeadsView : ContentView
	{

        public static readonly BindableProperty StatusProperty = BindableProperty.Create(nameof(Status), typeof(string), typeof(ViewLeadsView), string.Empty, BindingMode.TwoWay, null, OnTextPropertyChanged, null, null, null);

        public string Status
        {
            get { return (string)GetValue(StatusProperty); }
            set { SetValue(StatusProperty, value);
               
            }
        }
        ViewLeadsViewModel viewModel;
        public ViewLeadsView ()
		{
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            

        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == StatusProperty.PropertyName && !String.IsNullOrEmpty(Status))
            {
                viewModel = new ViewLeadsViewModel(Navigation, Status);
                BindingContext = viewModel;
            }

        }

       

        private async void ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {

            var viewCellDetails = e.Item as LeadDetailsItem;
            int viewCellIndex = viewModel.getIndexOf(viewCellDetails);

            if (viewModel.Items.Count - 2 <= viewCellIndex)
            {
                if (!viewModel.NoFurtherData)
                {
                    await viewModel.LoadItemsWithPaging();
                }
            }

        }

        public static void OnTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            
        }

        private void ViewModelNew_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var leadDetailsItem = (LeadDetailsItem)((ListView)sender).SelectedItem;
            Navigation.PushAsync(new LeadInfoPage(leadDetailsItem.LeadId));
        }
    }
}