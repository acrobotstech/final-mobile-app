﻿using AcroMobileApp.Models;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RaiseMeetingsPage : ContentPage
	{
        RaiseMeetingViewModel viewModel;
        public RaiseMeetingsPage (int leadId)
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = new RaiseMeetingViewModel(Navigation, leadId);
            BindingContext = viewModel;
        }
	}
}