﻿using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChangeLeadStatusPage : ContentPage
	{

        ChangeLeadStatusViewModel viewModel;
        public ChangeLeadStatusPage (int leadId)
		{
			InitializeComponent ();

            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = new ChangeLeadStatusViewModel(Navigation, leadId);
            BindingContext = viewModel;
            viewModel.Status = "Withdrawn";
        }

        private void Withdrwan_Tapped(object sender, EventArgs e)
        {
            frameReject.BorderColor = Color.Black;
            lblReject.TextColor = Color.Black;
            frameWithdrawn.BorderColor = Color.FromHex("#fc610e");
            lblWithdrawn.TextColor = Color.FromHex("#fc610e");
            viewModel.Status = "Withdrawn";
        }

        private void Reject_Tapped(object sender, EventArgs e)
        {
            frameReject.BorderColor = Color.FromHex("#fc610e");
            lblReject.TextColor = Color.FromHex("#fc610e");
            frameWithdrawn.BorderColor = Color.Black;
            lblWithdrawn.TextColor = Color.Black;
            viewModel.Status = "Rejected";
        }
    }
}