using System;
using System.Collections.Generic;
using AcroMobileApp.ViewModels;
using Xamarin.Forms;

namespace AcroMobileApp.Views
{
    public partial class RegisterUserPage : ContentPage
    {
        public RegisterUserPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new RegisterUserViewModel(Navigation);

        }
    }
}