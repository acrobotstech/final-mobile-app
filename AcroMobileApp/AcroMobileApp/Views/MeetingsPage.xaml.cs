﻿using acrobots.mobile;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MeetingsPage : ContentPage
    {

        MeetingsViewModel viewModel { get; set; }
      
        public MeetingsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = new MeetingsViewModel(Navigation);
            BindingContext = viewModel;
        }
       
        async void OnBackImageButtonClicked(object sender, EventArgs e)
        {

            await Navigation.PopAsync();

        }

        private void Date_DateSelectedAsync(object sender, DateChangedEventArgs e)
        {
          


        }

        private async System.Threading.Tasks.Task onDateChange()
        {
            await viewModel.ReloadItems();

        }

        private void Date_Unfocused(object sender, FocusEventArgs e)
        {
            onDateChange();
        }
    }
}
