﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcroMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginWithOtpPage : ContentPage
	{
        public LoginWithOtpPage (INavigation navigation, string mobileNumber)
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new LoginWithOtpViewModel(navigation, mobileNumber);
		}

        void Entry_TextChanged1(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;
            if (entry.Text != "")
                txtOtp2.Focus();
        }

        void Entry_TextChanged2(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;
            if (entry.Text != "")
                txtOtp3.Focus();
        }

        void Entry_TextChanged3(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;
            if (entry.Text != "")
                txtOtp4.Focus();
        }

        void Entry_TextChanged4(object sender, TextChangedEventArgs e)
        {
            btnVerify.Focus();
        }

        protected override void OnDisappearing()
        {
            BindingContext = null;
            Content = null;
            base.OnDisappearing();
            GC.Collect();
        }
    }
}