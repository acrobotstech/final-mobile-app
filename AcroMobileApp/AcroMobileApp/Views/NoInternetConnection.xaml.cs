﻿using acrobots.mobile;
using AcroMobileApp.BusinessLayer;
using AcroMobileApp.Util;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NoInternetConnection : ContentPage
	{

        public Command RetryCommand { get; set; }
        public bool IsChecking { get; set; }
        public NoInternetConnection ()
		{
			InitializeComponent ();
            RetryCommand = new Command(async () => retry());
            BindingContext = this;
          
        }

        private void retry()
        {
            IsChecking = true;
            if (CrossConnectivity.Current.IsConnected)
            {
                if (ValidateToken())
                {

                    GotoHomePage();

                }
                else
                    GotoLoginPage();
                IsBusy = false;
            }
            else
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage("No internet connection."));
                IsBusy = false;
            }
        }


        private bool ValidateToken()
        {
            return BusinessManager.ValidateToken();
        }

        public static void GotoLoginPage()
        {
            App.Current.MainPage = new NavigationPage(new LoginPage());
        }

        public static void GotoHomePage()
        {
            App.Current.MainPage = new NavigationPage(new MainPage());
        }
    }
}