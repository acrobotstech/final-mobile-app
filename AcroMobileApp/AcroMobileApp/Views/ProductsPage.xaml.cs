﻿using acrobots.mobile;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
    public partial class ProductsPage : ContentPage
    {
        ProductViewModel viewModel { get; set; }
        public ProductsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = new ProductViewModel(Navigation);
            BindingContext = viewModel;
        }
    }
}
