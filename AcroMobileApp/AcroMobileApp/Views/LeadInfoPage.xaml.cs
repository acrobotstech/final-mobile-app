﻿using acrobots.mobile;
using AcroMobileApp.Controls;
using AcroMobileApp.Models;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LeadInfoPage : ContentPage
    {

        LeadInfoViewModel ViewModel { get; set; }
        int LeadId;
        public LeadInfoPage(int leadId)
        {
            InitializeComponent();
            LeadId = leadId;
            NavigationPage.SetHasNavigationBar(this, false);
            
            try
            {
                ViewModel = new LeadInfoViewModel(Navigation, leadId);
                BindingContext = ViewModel;
            }
            catch (ApiCallFailedException ex)
            {
                Device.BeginInvokeOnMainThread(() => DependencyService.Get<IDisplayMessage>().DisplayMessage("Internal server error."));

            }
        }


        void Entry_TextChanged1(object sender, TextChangedEventArgs e)
        {
            
        }


        private void ViewDcouments_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ViewLeadDocsPage(LeadId));
        }

        private void FillApplication_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ApplicationFillPage(Navigation, LeadId));
        }

        private void ChangeStatus_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ChangeLeadStatusPage(LeadId));
        }

        private void Meetings_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RaiseMeetingsPage(LeadId));
        }

        private void ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            if (ViewModel.AllNotes != null)
            viewModelNew.HeightRequest = (ViewModel.AllNotes.Count + 1) * 40;           

        }

        private void Entry_Focused(object sender, FocusEventArgs e)
        {
            btnSave.IsVisible = true;
            MainScroll.ScrollToAsync((GradientButtonView)btnSave, ScrollToPosition.End, true);
        }
    }
}