﻿using acrobots.mobile;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
    public partial class EarningsPage : ContentPage
    {
        EarningViewModel viewModel { get; set; }
        public EarningsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = new EarningViewModel(Navigation);
            BindingContext = viewModel;
        }
    }
}
