﻿using AcroMobileApp.Models;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ViewLeadsPage : ContentPage
	{
        ViewLeadsPageModel viewModel;
        public ViewLeadsPage (Boolean isShowAll)
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel =new ViewLeadsPageModel(Navigation, isShowAll);
            BindingContext = viewModel;
        }

        async void OnBackImageButtonClicked(object sender, EventArgs e)
        {

            await Navigation.PopAsync();

        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            this.InProgressView.Status = "InProgress";
            this.LoginView.Status = "Login";
            this.RejectedView.Status = "Rejected";
            this.DisbursedView.Status = "Disbursed";
        }




    }
}