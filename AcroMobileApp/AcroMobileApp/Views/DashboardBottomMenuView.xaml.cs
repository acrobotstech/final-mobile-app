﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DashboardBottomMenuView : ContentView
	{
        public ICommand TabIndexChangedCommand { get; }
        public DashboardBottomMenuView ()
		{
			InitializeComponent ();
            TabIndexChangedCommand = new Command((parameter) => TabIndexChanged(parameter));
            BindingContext = this;

        }

        private void TabIndexChanged(object parameter)
        {
            onMenuClick(parameter.ToString());
        }

        void OnImageButtonClicked(object sender, EventArgs e)
        {
            var button = (ImageButton)sender;
            var classId = button.ClassId;

            onMenuClick(classId);



        }

        private void onMenuClick(String classId)
        {
            switch (classId)
            {

                case "DASHBOARD":
                    Navigation.PushAsync(new DashboardPage());
                    break;
                case "PRODUCTS":
                    Navigation.PushAsync(new ProductsPage());
                    break;
                case "EARNINGS":
                    Navigation.PushAsync(new EarningsPage());
                    break;
                case "VIEWLEADS":

                    Navigation.PushAsync(new ViewLeadsPage(true));
                    break;
                case "CREATELEAD":

                    Navigation.PushAsync(new CreateLeadPage1());
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }
    }
}