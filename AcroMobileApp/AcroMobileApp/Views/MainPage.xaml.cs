﻿using AcroMobileApp.Models;
using AcroMobileApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();
        DashboardViewModel viewModel;
        public MainPage()
        {
            InitializeComponent();

            viewModel = new DashboardViewModel();
            BindingContext = viewModel;
            MasterBehavior = MasterBehavior.Popover;

            IsPresented = false;

            
        }

        void OnImageButtonClicked(object sender, EventArgs e)
        {
            IsPresented = !IsPresented;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.DoOnFirstAppearanceAsync(viewModel.FetchAllData);
        }

        public async Task NavigateFromMenu(int id)
        {
            if (!MenuPages.ContainsKey(id))
            {
                
                switch (id)
                {
                    case (int)MenuItemType.AboutUs:
                        MenuPages.Add(id, new NavigationPage(new AboutPage()));
                        break;
                    case (int)MenuItemType.Earnings:
                        MenuPages.Add(id, new NavigationPage(new EarningsPage()));
                        break;
                    case (int)MenuItemType.Calculators:
                        MenuPages.Add(id, new NavigationPage(new CalculatorsPage()));
                        break;
                    case (int)MenuItemType.MyLeads:
                        MenuPages.Add(id, new NavigationPage(new ViewLeadsPage(false)));
                        break;
                    case (int)MenuItemType.Notification:
                        MenuPages.Add(id, new NavigationPage(new NotificationsPage()));
                        break;
                    case (int)MenuItemType.Products:
                        MenuPages.Add(id, new NavigationPage(new ProductsPage()));
                        break;
                    case (int)MenuItemType.Meetings:
                        MenuPages.Add(id, new NavigationPage(new MeetingsPage()));
                        break;
                    case (int)MenuItemType.ContactUs:
                        MenuPages.Add(id, new NavigationPage(new ContactUsPage()));
                        break;
                    case (int)MenuItemType.Logout:
                        MenuPages.Add(id, new NavigationPage(new AboutPage()));
                        break;
                }


                
            }

            var newPage = MenuPages[id];

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(100);

                IsPresented = false;
            }
        }
    }
}