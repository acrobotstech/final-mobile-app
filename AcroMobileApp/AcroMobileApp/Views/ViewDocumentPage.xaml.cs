﻿using System;
using System.Collections.Generic;
using AcroMobileApp.ViewModels;
using Xamarin.Forms;

namespace AcroMobileApp.Views
{
    public partial class ViewDocumentPage : ContentPage
    {
        public ViewDocumentPage(INavigation navigation, string documentSource)
        {
            InitializeComponent();
            BindingContext = new ViewDocumentViewModel(navigation, documentSource);
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
