﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AcroMobileApp.ViewModels;
using Xamarin.Forms;

namespace AcroMobileApp.Views
{
    public partial class CreateLeadPage1 : ContentPage
    {
        CreateLeadViewModel viewModel;
        public CreateLeadPage1()
        {
            InitializeComponent();
            viewModel = new CreateLeadViewModel(Navigation);
            BindingContext = viewModel;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.IsFirstAppearance)
                await viewModel.GetProductTypes();
        }
    }
}
