﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcroMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AcroMobileApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DashboardPage : ContentPage
	{
        DashboardViewModel viewModel;
		public DashboardPage ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = new DashboardViewModel();
            BindingContext = viewModel;
		}

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await viewModel.DoOnFirstAppearanceAsync(viewModel.FetchAllData);
        }
    }
}