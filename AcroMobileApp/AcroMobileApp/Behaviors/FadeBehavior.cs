﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using Xamarin.Forms;

namespace AcroMobileApp.Behaviors
{
    /// <summary>
    /// Behaviour which fades in a view when its IsVisible property is set to true.
    /// </summary>
    public class FadeVisibleBehavior : Behavior<View>
    {
        protected override void OnAttachedTo(View bindable)
        {
            bindable.PropertyChanging += Bindable_PropertyChanging;
        }

        protected override void OnDetachingFrom(View bindable)
        {
            bindable.PropertyChanging -= Bindable_PropertyChanging;
        }

        /// <summary>
        /// Minimum opacity of the fade. 0.0 means full transparent.
        /// </summary>
        public static readonly BindableProperty FromProperty = BindableProperty.Create("From", typeof(double), typeof(FadeVisibleBehavior), 0.0);

        public double From
        {
            get { return (double)GetValue(FromProperty); }
            set { SetValue(FromProperty, value); }
        }

        /// <summary>
        /// Maximum opaciy of the fade. 1.0 means fully opaque.
        /// </summary>
        public static readonly BindableProperty ToProperty = BindableProperty.Create("To", typeof(double), typeof(FadeVisibleBehavior), 1.0);

        public double To
        {
            get { return (double)GetValue(ToProperty); }
            set { SetValue(ToProperty, value); }
        }

        /// <summary>
        /// Duration of the fade in ms
        /// </summary>
        public static readonly BindableProperty LengthProperty = BindableProperty.Create("Length", typeof(uint), typeof(FadeVisibleBehavior), 2000U);

        public uint Length
        {
            get { return (uint)GetValue(LengthProperty); }
            set { SetValue(LengthProperty, value); }
        }


        async void Bindable_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (e.PropertyName.Equals(View.IsVisibleProperty.PropertyName))
            {
                var vw = sender as View;

                if (vw != null)
                {
                    if (!vw.IsVisible)
                    {
                        // View is not visible (so about to become visible. Fade in.
                        Debug.WriteLine("Fading in");
                        await vw.FadeTo(To, Length);
                    }
                    else
                    {
                        // View is visible (so about to become not visible. Fade out.
                        // NOTE - This doesn't work because the view becomes invisible immediately
                        // after this method is called, so the fade out is not seen. Needs a rethink
                        // if we were ever to use this. At the moment we don't use it so no problem.
                        Debug.WriteLine("Fading out");
                        await vw.FadeTo(From, Length);
                    }
                }
            }
        }
    }
}