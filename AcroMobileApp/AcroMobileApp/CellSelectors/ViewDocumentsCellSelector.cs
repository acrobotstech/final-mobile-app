﻿using System;
using AcroMobileApp.ViewModels;
using AcroMobileApp.Views.ContentViews;
using Xamarin.Forms;

namespace AcroMobileApp.CellSelectors
{
    public class ViewDocumentsCellSelector : DataTemplateSelector
    {
        private readonly DataTemplate categoryView;
        private readonly DataTemplate fileDetailsView;
        private readonly DataTemplate separatorView;
        private readonly DataTemplate emptyView;

        public ViewDocumentsCellSelector()
        {
            categoryView = new DataTemplate(typeof(CategoryView));
            fileDetailsView = new DataTemplate(typeof(FileDetailsView));
            separatorView = new DataTemplate(typeof(SeparatorView));
            emptyView = new DataTemplate(typeof(EmptyView));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            Type type = item.GetType();

            if (item is FileCategoryViewModel)
                return categoryView;

            if (item is FileDetailsViewModel)
                return fileDetailsView;

            if (item is SeparatorCellViewModel)
                return separatorView;

            if (item is EmptyCellViewModel)
                return emptyView;

            return null;
        }
    }
}
