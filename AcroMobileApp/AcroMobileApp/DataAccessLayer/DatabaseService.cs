﻿using System;
using System.Collections.Generic;
using SQLite;
using SQLiteAsync.Services;
using SQLiteNetExtensions.Extensions;
using AcroMobileApp.BusinessLayer.Contracts;
using Xamarin.Forms;
using System.Linq;

[assembly: Dependency(typeof(DatabaseService))]
namespace SQLiteAsync.Services
{
    [Xamarin.Forms.Internals.Preserve(AllMembers = true)]
    public class DatabaseService
    {
        private static DatabaseService _instance;

        public DatabaseService()
        {
        }

        private SQLiteConnection Database { get; } = DependencyService.Get<ISQLite>().GetConnection();

        public static DatabaseService Instance => _instance ?? (_instance = new DatabaseService());

        public void AddItem<T>(T item) where T :  new()
        {
            Database.DeleteAll<T>();
            Database.InsertWithChildren(item);
        }

        public void DeleteAll<T>(T item) where T : BusinessEntityBase, new()
        {
            Database.DeleteAll<T>();
        }

        public void AddItems<T>(IList<T> items) where T : BusinessEntityBase, new()
        {
            // Delete everything the add the items in
            Database.DeleteAll<T>();
            Database.InsertAllWithChildren(items);
        }

        public List<T> GetAllItems<T>() where T : BusinessEntityBase, new()
        {
            var data = Database.GetAllWithChildren<T>().ToList();
            return data;
        }
    }
}
