﻿using SQLite;
using System.Linq;

namespace SQLiteAsync
{
    [Xamarin.Forms.Internals.Preserve(AllMembers = true)]
    public abstract class SQLiteBase : ISQLite
    {
        protected static readonly string PRAGMA_KEY_NAME = "PragmaKey";

        public SQLiteConnection GetConnection()
        {
            var dbPath = GetDatabasePath();

            // Return the synchronous database connection 
            var db = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex, true);

            //The "PRAGMA key" is the command SQLCipher Encryption key
            // xyz1921 is the passphrase for encrypting and decrypting the Database
            //This statement will not raise exception if the key is wrong
            string pragmaKey = getPragmaKey(); // Assign first. Helpful in debugger
            var t = db.Query<int>(@"PRAGMA key=" + "'" + pragmaKey + "'");

            //This will try to query the SQLite Schema Database, if the key is correct then no error is raised
            var tt = db.Query<int>("SELECT count(*) FROM sqlite_master");

            return db;
        }

        /// <summary>
        /// Delete the database.
        /// Requires System.IO.File which is not available in PCL so
        /// abstract here and implemented in platform specific code.
        /// </summary>
        public abstract void DeleteDatabase();

        protected static string generatePragmaKey()
        {
            System.Random random = new System.Random();
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 15).Select(s => s[random.Next(s.Length)]).ToArray());
        }



        abstract protected string GetDatabasePath();

        abstract protected string getPragmaKey();
    }
}
