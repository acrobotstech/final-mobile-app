﻿using System;
namespace AcroMobileApp
{
    public interface IDisplayMessage
    {
        void DisplayMessage(string message);
    }
}
