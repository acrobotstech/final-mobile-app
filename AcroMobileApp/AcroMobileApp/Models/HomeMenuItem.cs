﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AcroMobileApp.Models
{
    public enum MenuItemType
    {
        MyLeads,
        Notification,
        Calculators,
        Products,
        Meetings,
        Earnings,
        AboutUs,
        ContactUs,
        Logout

    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }

        public string Icon  { get; set; }
    }
}
