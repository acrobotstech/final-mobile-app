﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AcroMobileApp.Models
{
   public class MeetingItems
    {
        public DateTime Date { get; set; }
        public string TimeSlot { get; set; }
        public string Address { get; set; }
        public int LeadId { get; set; }
        public string MeetingDate { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
    }
}
