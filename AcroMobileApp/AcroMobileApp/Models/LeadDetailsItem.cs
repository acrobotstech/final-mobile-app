﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AcroMobileApp.Models
{
    public class LeadDetailsItem
    {

    
        public string Name { get; set; }
        public int LeadId { get; set; }
        public string Product { get; set; }
        public string Amount { get; set; }
        public string CreatedDateTime { get; set; }
        public string MobileNo { get; set; }
        public string CreatedDays { get; set; }
    }
}