﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDetailsExample.Models
{
    public class Geo
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }
}
