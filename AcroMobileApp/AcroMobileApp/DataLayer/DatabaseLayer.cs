﻿using System;
using System.Collections.Generic;
using acrobots.mobile.BusinessLayer.Model;
using AcroMobileApp.BusinessLayer.Model;
using SQLite;
using SQLiteAsync;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace AcroMobileApp.DataLayer
{
    [Xamarin.Forms.Internals.Preserve(AllMembers = true)]
    public static class DatabaseLayer
    {
        public static void CreateAllTables()
        {
            SQLiteConnection dbConnection = null;
            try
            {
                dbConnection = DependencyService.Get<ISQLite>().GetConnection();
            }
            catch(Exception ex)
            {
                DependencyService.Get<ISQLite>().DeleteDatabase();
                dbConnection = DependencyService.Get<ISQLite>().GetConnection();
            }

            dbConnection.CreateTable<Auth>();
        }

        public static void DropAllTables()
        {
            var dbConnection = DependencyService.Get<ISQLite>().GetConnection();
            // call DROP TABLE on every table name
            foreach (TableName table in GetAllTables())
            {
                var dropQuery = "DROP TABLE IF EXISTS " + table.Name;
                dbConnection.ExecuteScalar<int>(dropQuery);
            }

            dbConnection.Dispose();
        }

        public static void TruncateAllTables()
        {
            var dbConnection = DependencyService.Get<ISQLite>().GetConnection();
            // call DROP TABLE on every table name
            foreach (TableName table in GetAllTables())
            {
                var dropQuery = "DELETE FROM " + table.Name;
                dbConnection.ExecuteScalar<int>(dropQuery);
            }
            dbConnection.Dispose();
        }

        private static List<TableName> GetAllTables()
        {
            var dbConnection = DependencyService.Get<ISQLite>().GetConnection();
            List<TableName> tables = dbConnection.Query<TableName>("SELECT name FROM sqlite_master WHERE type='table'");
            dbConnection.Dispose();
            return tables;
        }
    }
}