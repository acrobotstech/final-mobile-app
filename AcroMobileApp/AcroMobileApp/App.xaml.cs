﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AcroMobileApp.Views;
using AcroMobileApp.DataLayer;
using AcroMobileApp.BusinessLayer;
using System.Net.NetworkInformation;
using AcroMobileApp.Util;
using Plugin.Connectivity;
using acrobots.mobile;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AcroMobileApp
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();
            
            DatabaseLayer.CreateAllTables();
            CheckConnection();
                   
        }




        private async void CheckConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
                App.Current.MainPage = new NavigationPage(new NoInternetConnection());
            else
            {
                if (ValidateToken())
                {

                    GotoHomePage();

                }
                else
                    GotoLoginPage();
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        private bool ValidateToken()
        {
            return true;// BusinessManager.ValidateToken();
        }

        public static void GotoLoginPage()
        {
            App.Current.MainPage = new NavigationPage(new LoginPage());
        }

        public static void GotoHomePage()
        {
            App.Current.MainPage = new NavigationPage(new MainPage());
        }
    }
}