﻿using System;
using AcroMobileApp.iOS.Messaging;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ConcreteToastFactory))]
namespace AcroMobileApp.iOS.Messaging
{
    public class ConcreteToastFactory : IDisplayMessage
    {
        public void DisplayMessage(string message)
        {
            var snackbar = new TTGSnackbar(" " + message);
            snackbar.LocationType = TTGSnackbarLocation.Top;
            snackbar.Duration = TimeSpan.FromSeconds(6);
            snackbar.BackgroundColor = UIColor.Orange;
            snackbar.ActionButton.Hidden = true;
            snackbar.Show();
        }
    }
}
