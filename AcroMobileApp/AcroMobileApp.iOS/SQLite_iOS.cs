﻿using System;
using SqliteAsync.iOS;
using Xamarin.Forms;
using System.IO;
using Security;
using SQLiteAsync;
using Foundation;
using System.Diagnostics;

[assembly: Dependency(typeof(SQLite_iOS))]

namespace SqliteAsync.iOS
{
    /// <summary>
    /// iOS-specific wrapper for SqLite
    /// </summary>
    [Preserve(AllMembers = true)]
    public class SQLite_iOS : SQLiteBase
    {
        //Generates the key used to access the database, for Keychain info please check: https://developer.apple.com/documentation/security/keychain_services?language=objc
        protected override string getPragmaKey()
        {
            // Try to get the existing pragma key
            SecStatusCode res;
            var match = SecKeyChain.QueryAsRecord(createSecRecord(), out res);
            if (res == SecStatusCode.Success)
            {
                return match.ValueData.ToString();
            }

            // Didn't find an existing pragma key, so create a new one
            var key = generatePragmaKey();

            var newRec = createSecRecord(NSData.FromString(key, NSStringEncoding.UTF8));
            var err = SecKeyChain.Add(newRec);
            /*if (err != SecStatusCode.Success && err != SecStatusCode.DuplicateItem)
            {
                throw new Exception("Could not get or create pragma key");
            }*/

            return key;

        }

        public override void DeleteDatabase()
        {
            // Delete the DB file
            var path = GetDatabasePath();
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            // Delete the encrypted pragma key
            SecKeyChain.Remove(createSecRecord());

        }


        protected override string GetDatabasePath()
        {
            const string sqliteFilename = "SelfServiceDb.db3";

            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
            var libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
            var pathFull = Path.Combine(libraryPath, sqliteFilename);

            // It's useful to know this, so you can explore the database with something like SQLiteBrowser
            Debug.WriteLine(pathFull);

            return pathFull;
        }

        /// <summary>
        /// Create SecRecord instance to lookup or populate the pragma key entry in the key chain
        /// </summary>
        /// <param name="valueData"></param>
        /// <returns></returns>
        private static SecRecord createSecRecord(NSData valueData = null)
        {
            SecRecord rec = new SecRecord(SecKind.GenericPassword)
            {
                Label = "Pragma Key",
                Description = "Pragma Key"
            };
            if (valueData != null)
            {
                rec.ValueData = valueData;
            }
            return rec;
        }
    }
}